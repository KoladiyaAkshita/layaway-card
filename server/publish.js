import { Meteor } from 'meteor/meteor';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { check, Match } from 'meteor/check';

/**
 * https://atmospherejs.com/alanning/roles#roles-installing
 */
/**
 * Profiles
 */
import { Profiles } from '../imports/startup/both/collections/profiles.js';

/**
 * AuthTokens
 */
import { AuthTokens } from '../imports/startup/both/collections/auth-tokens.js';
import CryptoJS from 'crypto-js';

// Meteor.publish('authToken', function (id) {
//   check(id, String);

//   var authToken = AuthTokens.findOne({ _id: id }, {
//     fields: {
//       _id: 1,
//       applicationName: 1,
//       url: 1,
//       server: 1,
//       publicKey: 1,
//       privateKey: 1  // TODO: This property should not be published
//     }
//   });
//   var cryptoJs = CryptoJS.AES.decrypt(authToken.privateKey.toString(), Meteor.userId());
//   var decryptedPrivateKey = cryptoJs.toString(CryptoJS.enc.Utf8);

//   authToken.privateKey = '***************************' + decryptedPrivateKey.substr(28, 4);

//   var DecryptedAuthTokens = new Mongo.Collection(null);

//   DecryptedAuthTokens.insert(authToken);

//   return DecryptedAuthTokens.find();
// });

import { UserSession } from 'meteor/benjaminrh:user-session';

/**
 *Companies
 */
import { Companies } from '../imports/startup/both/collections/companies.js';

/**
 *ActivityLogs
 */
import { ActivityLogs } from '../imports/startup/both/collections/activity-logs.js';

/**
 * NOTE: Roles packages was upgraded to version 3.
 * https://github.com/Meteor-Community-Packages/meteor-roles#changes-to-default-meteor-behavior
 */
Meteor.publish(null, function () {
  if (this.userId) {
    // return Meteor.roleAssignment.find({ 'user._id': this.userId });
    return Meteor.roleAssignment.find();
  } else {
    this.ready();
  }
});

Meteor.publishComposite('userProfile', function (userId) {
  check(userId, String);

  return {
    find: function () {
      return Profiles.find({ userId: userId });
    }
  };
});

Meteor.publishComposite('profile', function (profileId) {
  check(profileId, String);

  return {
    find: function () {
      const profile = Profiles.findOne({ userId: this.userId });
      const role = profile ? profile.role() : '';

      if (typeof profile === 'undefined' || profile === null || profile === '') {
        return this.ready();
      } else if ((role === 'Admin') || (role === 'CompanyAdmin')) {
        // NOTE: Return both the requested and the admin's profile
        return Profiles.find({ _id: { $in: [profileId, profile.companyId] } });
      } else {
        return Profiles.find({ _id: profileId });
      }
    },
    children: [
      {
        find: function (profile) {
          return Meteor.users.find({ _id: profile.userId }, {
            fields: {
              _id: 1,
              roles: 1
            }
          });
        }
      },
      {
        find: function (profile) {
          return Companies.find( { _id: profile. companyId }, { fields: { name: 1 } } );
        }
      }
    ]
  };
});

Meteor.publishComposite('profiles', function () {
  return {
    find: function () {
      const profile = Profiles.findOne({ userId: Meteor.userId() });
      const role = profile ? profile.role() : '';

      if (typeof profile === 'undefined' || profile === null || profile === '') {
        return this.ready();
      } else if (role === 'Admin') {
        console.log('publishing Profiles for Admin...');

        return Profiles.find();
      } else if (role === 'CompanyAdmin') {
        console.log('publishing Profiles for CompanyAdmin...');

        return Profiles.find({ companyId: profile.companyId });
      }

      return this.stop();
    },
    children: [
      {
        find: function (profile) {
          return Meteor.users.find({ _id: profile.userId }, {
            fields: {
              _id: 1,
              roles: 1
            }
          });
        }
      },
      {
        find: function (profile) {
          return Companies.find({ _id: profile.companyId }, {
            fields: {
              _id: 1,
              name: 1
            }
          })
        }
      }
    ]
  };
});

Meteor.publish('authToken', function (id) {
  check(id, String);

  var authToken = AuthTokens.findOne({ _id: id }, {
    fields: {
      privateKey: 1 // TODO: This property should not be published
    }
  });

  var cryptoJs = CryptoJS.AES.decrypt(authToken.privateKey.toString(), Meteor.userId());
  var decryptedPrivateKey = cryptoJs.toString(CryptoJS.enc.Utf8);

  authToken.privateKey = '***************************' + decryptedPrivateKey.substr(28, 4);
  UserSession.set('decryptedPrivateKey', authToken.privateKey, Meteor.userId());
  // console.log(authToken);

  return AuthTokens.find({ _id: id }, {
    fields: {
      _id: 1,
      applicationName: 1,
      url: 1,
      publicKey: 1
    }
  });
});

Meteor.publishComposite('paginatedAuthTokens', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
    check(x, Match.Integer);
    return x >= 0;
  });

  check(skipCount, positiveIntegerCheck);
  // check(search, Match.OneOf(String, null, undefined));

  // var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  // if (typeof search !== 'undefined' && search !== null && search !== '') {
  //  query.search = regexSearch;
  // }

  Counts.publish(this, 'authTokensCount', AuthTokens.find(query), { noReady: true });

  return {
    find: function () {
      return AuthTokens.find(query, options);
    }
  };
});

Meteor.publish('companiesForUser', function (userId) {
  check(userId, String);

  return Companies.find({ createdBy: userId }, { fields: {} });
});

Meteor.publish('companies', function () {
  return Companies.find({}, { fields: {} });
});

Meteor.publish('company', function (id) {
  check(id, String);

  return Companies.find({ _id: id }, { fields: {} });
});

Meteor.publishComposite('paginatedCompanies', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
    check(x, Match.Integer);
    return x >= 0;
  });

  check(skipCount, positiveIntegerCheck);
  // check(search, Match.OneOf(String, null, undefined));

  // var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  // if (typeof search !== 'undefined' && search !== null && search !== '') {
  //  query.search = regexSearch;
  // }

  Counts.publish(this, 'companiesCount', Companies.find(query), { noReady: true });

  return {
    find: function () {
      return Companies.find(query, options);
    }
  };
});

Meteor.publishComposite('paginatedCompaniesForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
    check(x, Match.Integer);
    return x >= 0;
  });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  // check(search, Match.OneOf(String, null, undefined));

  // var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  // if (typeof search !== 'undefined' && search !== null && search !== '') {
  //  query.search = regexSearch;
  // }

  Counts.publish(this, 'companiesCount', Companies.find(query), { noReady: true });

  return {
    find: function () {
      return Companies.find(query, options);
    }
  };
});

Meteor.publish('activityLogsForUser', function (userId) {
  check(userId, String);

  return ActivityLogs.find({ createdBy: userId }, { fields: {} });
});

Meteor.publish('activityLogs', function () {
  return ActivityLogs.find({}, { fields: {} });
});

Meteor.publish('activityLog', function (id) {
  check(id, String);

  return ActivityLogs.find({ _id: id }, { fields: {} });
});

Meteor.publishComposite('paginatedActivityLogs', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
    check(x, Match.Integer);
    return x >= 0;
  });

  check(skipCount, positiveIntegerCheck);
  // check(search, Match.OneOf(String, null, undefined));

  // var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  // if (typeof search !== 'undefined' && search !== null && search !== '') {
  //  query.search = regexSearch;
  // }

  Counts.publish(this, 'activityLogsCount', ActivityLogs.find(query), { noReady: true });

  return {
    find: function () {
      return ActivityLogs.find(query, options);
    }
  };
});

Meteor.publishComposite('paginatedActivityLogsForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
    check(x, Match.Integer);
    return x >= 0;
  });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  // check(search, Match.OneOf(String, null, undefined));

  // var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  // if (typeof search !== 'undefined' && search !== null && search !== '') {
  //  query.search = regexSearch;
  // }

  Counts.publish(this, 'activityLogsCount', ActivityLogs.find(query), { noReady: true });

  return {
    find: function () {
      return ActivityLogs.find(query, options);
    }
  };
});
/**
 *Cards
 */

import { Cards } from '../imports/startup/both/collections/cards.js';

Meteor.publish('cardsForUser', function (userId) {
  check(userId, String);

  return Cards.find({ createdBy: userId }, { fields: {}});
});

Meteor.publish('cards', function () {
  return Cards.find({}, { fields: {}});
});

Meteor.publish('card', function (id) {
  check(id, String);

  return Cards.find({ _id: id }, { fields: {}});
});

Meteor.publishComposite('paginatedCards', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'cardsCount', Cards.find(query), { noReady: true });

  return {
    find: function () {
      return Cards.find(query, options);
    }
  }
});

Meteor.publishComposite('paginatedCardsForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'cardsCount', Cards.find(query), { noReady: true });

  return {
    find: function () {
      return Cards.find(query, options);
    }
  }
});

/**
 *LayawayItems
 */

import { LayawayItems } from '../imports/startup/both/collections/layaway-items.js';

Meteor.publish('layawayItemsForUser', function (userId) {
  check(userId, String);

  return LayawayItems.find({ createdBy: userId }, { fields: {}});
});

Meteor.publish('layawayItems', function () {
  return LayawayItems.find({}, { fields: {}});
});

Meteor.publish('layawayItem', function (id) {
  check(id, String);

  return LayawayItems.find({ _id: id }, { fields: {}});
});

Meteor.publishComposite('paginatedLayawayItems', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'layawayItemsCount', LayawayItems.find(query), { noReady: true });

  return {
    find: function () {
      return LayawayItems.find(query, options);
    }
  }
});

Meteor.publishComposite('paginatedLayawayItemsForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'layawayItemsCount', LayawayItems.find(query), { noReady: true });

  return {
    find: function () {
      return LayawayItems.find(query, options);
    }
  }
});

/**
 *Items
 */

import { Items } from '../imports/startup/both/collections/items.js';

Meteor.publish('itemsForUser', function (userId) {
  check(userId, String);

  return Items.find({ createdBy: userId }, { fields: {}});
});

Meteor.publish('items', function () {
  return Items.find({}, { fields: {}});
});

Meteor.publish('item', function (id) {
  check(id, String);

  return Items.find({ _id: id }, { fields: {}});
});

Meteor.publishComposite('paginatedItems', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'itemsCount', Items.find(query), { noReady: true });

  return {
    find: function () {
      return Items.find(query, options);
    }
  }
});

Meteor.publishComposite('paginatedItemsForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'itemsCount', Items.find(query), { noReady: true });

  return {
    find: function () {
      return Items.find(query, options);
    }
  }
});

/**
 *PaymentPlans
 */

import { PaymentPlans } from '../imports/startup/both/collections/payment-plans.js';

Meteor.publish('paymentPlansForUser', function (userId) {
  check(userId, String);

  return PaymentPlans.find({ createdBy: userId }, { fields: {}});
});

Meteor.publish('paymentPlans', function () {
  return PaymentPlans.find({}, { fields: {}});
});

Meteor.publish('paymentPlan', function (id) {
  check(id, String);

  return PaymentPlans.find({ _id: id }, { fields: {}});
});

Meteor.publishComposite('paginatedPaymentPlans', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'paymentPlansCount', PaymentPlans.find(query), { noReady: true });

  return {
    find: function () {
      return PaymentPlans.find(query, options);
    }
  }
});

Meteor.publishComposite('paginatedPaymentPlansForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'paymentPlansCount', PaymentPlans.find(query), { noReady: true });

  return {
    find: function () {
      return PaymentPlans.find(query, options);
    }
  }
});

/**
 *Payments
 */

import { Payments } from '../imports/startup/both/collections/payments.js';

Meteor.publish('paymentsForUser', function (userId) {
  check(userId, String);

  return Payments.find({ createdBy: userId }, { fields: {}});
});

Meteor.publish('payments', function () {
  return Payments.find({}, { fields: {}});
});

Meteor.publish('payment', function (id) {
  check(id, String);

  return Payments.find({ _id: id }, { fields: {}});
});

Meteor.publishComposite('paginatedPayments', function (skipCount) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = {};
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'paymentsCount', Payments.find(query), { noReady: true });

  return {
    find: function () {
      return Payments.find(query, options);
    }
  }
});

Meteor.publishComposite('paginatedPaymentsForUser', function (skipCount, userId) {
  var positiveIntegerCheck = Match.Where(function (x) {
      check(x, Match.Integer);
      return x >= 0;
    });

  check(userId, String);
  check(skipCount, positiveIntegerCheck);
  //check(search, Match.OneOf(String, null, undefined));

  //var regexSearch = new RegExp(search, 'i');
  var query = { createdBy: userId };
  var options = {
    limit: Meteor.settings.public.recordsPerPage, // records to show per page
    skip: skipCount,
    fields: {},
    sort: {}
  };

  //if (typeof search !== "undefined" && search !== null && search !== "") {
  //  query.search = regexSearch;
  //}

  Counts.publish(this, 'paymentsCount', Payments.find(query), { noReady: true });

  return {
    find: function () {
      return Payments.find(query, options);
    }
  }
});


heroku login
heroku create layaway-card-dev
heroku buildpacks:set https://github.com/AdmitHub/meteor-buildpack-horse.git
heroku addons:create papertrail
heroku addons:create sendgrid
heroku config:set ROOT_URL=https://layaway-card-dev.herokuapp.com
heroku features:enable http-session-affinity
heroku config:add METEOR_SETTINGS="$(cat ./config/development/settings.json)"
heroku config:add MONGODB_URI="<%MONGODB_URI%>"

heroku pipelines:create layaway-card-pipeline -a layaway-card-dev -s development

heroku fork --from layaway-card-staging --to layaway-card
heroku config:set ROOT_URL=https://layaway-card.herokuapp.com --app layaway-card-staging

heroku pipelines:add layaway-card-pipeline -a layaway-card-staging -s staging

heroku fork --from layaway-card --to layaway-card
heroku config:set ROOT_URL=https://layaway-card.herokuapp.com --app layaway-card

heroku pipelines:add layaway-card-pipeline -a layaway-card -s production
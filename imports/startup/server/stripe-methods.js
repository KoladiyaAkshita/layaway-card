import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { moment } from 'meteor/momentjs:moment';
import { StripeAPI } from 'meteor/mrgalaxy:stripe';
import { _ } from 'meteor/underscore';

let Stripe = StripeAPI(Meteor.settings.private.stripe.liveSecretKey);

if (Meteor.isDevelopment) {
  Stripe = StripeAPI(Meteor.settings.private.stripe.testSecretKey);
}

Meteor.methods({
  createCustomer: function (firstName, lastName, email, customerType) {
    check(firstName, String);
    check(lastName, String);
    check(email, String);
    check(customerType, String);

    const handleCreateCustomer = Meteor.wrapAsync(Stripe.customers.create, Stripe.customers);
    const result = handleCreateCustomer({
      description: firstName + ' ' + lastName + ': ' + customerType,
      email: email
    });

    console.log(result);

    return result;
  },
  processPayment: function (charge) {
    if (charge.customer) {
      check(charge, {
        amount: Number,
        currency: String,
        description: String,
        customer: String
      });
    } else {
      check(charge, {
        amount: Number,
        currency: String,
        source: String,
        description: String,
        receipt_email: String
      });
    }

    const handleCharge = Meteor.wrapAsync(Stripe.charges.create, Stripe.charges);
    const payment = handleCharge(charge);

    return payment;
  },
  initialPayment: function (tokenId, firstName, lastName, email, charge) {
    check(tokenId, String);
    check(firstName, String);
    check(lastName, String);
    check(email, String);
    check(charge, {
      amount: Number,
      currency: String,
      description: String
    });

    let handle = Meteor.wrapAsync(Stripe.customers.create, Stripe.customers);
    const customer = handle({
      source: tokenId,
      email: email,
      description: firstName + ' ' + lastName
    });

    handle = Meteor.wrapAsync(Stripe.charges.create, Stripe.charges);

    const payment = handle({
      amount: charge.amount,
      currency: charge.currency,
      customer: customer.id,
      description: charge.description
    });

    return payment;
  },
  /**
   * https://stripe.com/docs/api/node#create_refund
   */
  refundAll: function (chargeId) {
    check(chargeId, String);

    const handleRefund = Meteor.wrapAsync(Stripe.refunds.create, Stripe.refunds);
    const options = {
      charge: chargeId,
      reason: 'requested_by_customer'
    };
    const refund = handleRefund(options);

    return refund;
  },
  createSubscription: async function (customerId, planId, coupon) {
    let results = '';
    const availablePlans = Meteor.settings.public.plans;
    const selectedPlan = _.find(availablePlans, function (plan) { return plan.planId === planId; });

    if (selectedPlan.limit !== 5) {
      selectedPlan.limit = 1;
    }

    if (coupon !== null && coupon !== '' && typeof coupon !== 'undefined') {
      try {
        results = await Stripe.customers.createSubscription(customerId, {
          plan: planId,
          coupon: coupon,
          quantity: selectedPlan.limit
        });
      } catch (error) {
        results.status = 'Stripe error on creating customer subscription.';
        results.error = error;
      }
    } else {
      try {
        results = await Stripe.customers.createSubscription(customerId, {
          plan: planId,
          quantity: selectedPlan.limit
        });
      } catch (error) {
        results.status = 'Stripe error on creating customer subscription.';
        results.error = error;
      }
    }

    return results;
  },
  addUserToSubscription: async function (userProfile) {
    let results = {};
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);

    try {
      results = await stripe.subscriptions.update(userProfile.subscriptionId, { quantity: userProfile.userCount += 1 });
    } catch (error) {
      results.status = 'Stripe Error on retrieving customer subscription.';
      results.error = error;
    }

    return results;
  },
  changeSubscripton: async function (oldPlan, newPlanId) {
    let results = {};
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);
    const subscription = await stripe.subscriptions.retrieve(oldPlan);

    // TODO: Move to Word document
    // 1. We need the users subscriptionId to retrieve all plans listed.
    // 2. We update the subscription by changing the planId to another listed under the subscription

    try {
      results = await stripe.subscriptions.update(oldPlan, {
        cancel_at_period_end: false,
        items: [{
          id: subscription.items.data[0].id,
          plan: newPlanId
        }]
      });
    } catch (error) {
      console.log(error);
    }

    return results;
  },
  createCoupon: async function (coupon) {
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);
    let results = {};

    // durationInMonths is required only if the duration is repeating.
    // We can provide an Id or have Stripe create one for us.

    if (coupon.amountOff > 0) {
      coupon.amountOff = coupon.amountOff * 100;
    }

    if (coupon.redeemBy !== '') {
      coupon.redeemBy = moment(coupon.redeemBy).format('X');
    }

    Object.keys(coupon).forEach((key) => (coupon[key] === '') && delete coupon[key]);

    try {
      results = await stripe.coupons.create({
        percent_off: coupon.percentageOff,
        amount_off: coupon.amountOff,
        currency: 'USD',
        duration: coupon.duration.toLowerCase(),
        duration_in_months: coupon.durationInMonths,
        max_redemptions: coupon.numberOfUses,
        id: coupon.code,
        redeem_by: coupon.redeemBy
      });
    } catch (error) {
      results.status = 'Stripe error on creating coupon';
      results.error = error.message;
    }

    return results;
  },
  deleteCoupon: async function (id) {
    const results = {};
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);

    stripe.coupons.del(id, function (error, results) {
      if (error) {
        results.error = error.message;
      } else {
        results.message = 'Coupon deleted';
      }
    });

    return results;
  },
  checkUserQuota: function (profile) {
    // const plan = profile.planId;
    const availablePlans = Meteor.settings.public.plans;
    const currentPlan = _.find(availablePlans, function (plan) { return plan.planId === profile.planId; });
    const totalUsers = profile.userCount;
    let limit = 0;

    if (currentPlan.limit !== '' || currentPlan.limit !== null || typeof currentPlan.limit !== 'undefined') {
      limit = currentPlan.limit;
    }

    if (totalUsers < limit || limit === -1) {
      return true;
    } else {
      return false;
    }
  },
  updateDefaultCard: async function (user, tokenId) {
    let results = {};
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);

    if (Meteor.isServer) {
      try {
        results = await stripe.customers.update(user.customerNumber, {
          source: tokenId.id
        });
      } catch (error) {
        results.error = error;
        results.status = 'Error on updating card.';
      }
    }

    return results;
  },
  deleteSingleUser: async function (userProfile) {
    let results = {};
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);

    try {
      results = await stripe.subscriptions.update(userProfile.subscriptionId, { quantity: userProfile.userCount -= 1 });
    } catch (error) {
      results.status = 'Stripe Error on deleting user.';
      results.error = error;
    }
    return results;
  },
  cancelSubscription: async function (userProfile) {
    const stripe = require('stripe')(Meteor.settings.private.stripe.testSecretKey);
    let request = {};

    try {
      request = await stripe.subscriptions.update(userProfile.subscriptionId, {
        cancel_at_period_end: true
      });
    } catch (error) {
      request.error = error || 'Error on subscription cancelation.';
      request.status = error;
    }

    return request;
  }
});

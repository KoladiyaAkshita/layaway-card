import { AWS } from 'meteor/peerlibrary:aws-sdk';
import { Meteor } from 'meteor/meteor';

let config = {
  // TODO: bucketName: Meteor.settings.private.d3s3.bucketName,
  // dirName: 'photos', /* optional */
  region: Meteor.settings.private.s3.region,
  accessKeyId: Meteor.settings.private.s3.awsAccessKeyId,
  secretAccessKey: Meteor.settings.private.s3.awsSecretAccessKey,
  // s3Url: 'https://s3.us-east-1.amazonaws.com/',
  apiVersion: '2006-03-01',
  correctClockSkew: true
};
// let bucket = Meteor.settings.private.d3s3.bucketName;

if (Meteor.settings.public.s3) {
  config = {
    // TODO: bucketName: Meteor.settings.private.s3.bucketName,
    // dirName: 'photos', /* optional */
    region: Meteor.settings.private.s3.region,
    accessKeyId: Meteor.settings.private.s3.awsAccessKeyId,
    secretAccessKey: Meteor.settings.private.s3.awsSecretAccessKey,
    // s3Url: 'https://s3.us-east-1.amazonaws.com/',
    apiVersion: '2006-03-01',
    correctClockSkew: true
  };
  // bucket = Meteor.settings.private.s3.bucketName;
}

Meteor.methods({
  's3.uploadFile' (file, directory) {
    config.dirName = directory;

    const s3Client = new AWS.S3(config);
    const response = s3Client
      .uploadFile(file, file.name)
      .then(data => console.log(data))
      .catch(err => console.error(err));

    return response;
  },
  's3.upload' (data, fileName) {
    // debugger;
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName,
      Key: fileName,
      Body: data
    };
    var uploadPromise = s3.upload(params).promise();

    uploadPromise.then(function (data) {
      console.log(`File uploaded successfully at ${data.Location}`);
      return data;
    }).catch(function (err) {
      console.log(err);
      throw new Meteor.Error('s3.uploadSync', err.message);
    });
  },
  's3.uploadSync' (data, fileName, acl) {
    var buf = Buffer.from(data.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName,
      Key: fileName,
      Body: buf,
      ContentEncoding: 'base64'
    };
    let response = {};

    if (acl === 'public-read') {
      params.ACL = 'public-read';
    }

    try {
      response = s3.uploadSync(params);
    } catch (err) {
      console.log(err);
      throw new Meteor.Error('s3.uploadSync', err.message);
    }

    return response;
  },
  's3.putObject' (data, fileName) {
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName,
      Key: fileName,
      Body: data
    };
    var putObjectPromise = s3.putObject(params).promise();

    putObjectPromise.then(function (data) {
      return data;
    }).catch(function (err) {
      console.log(err);
      throw new Meteor.Error('s3.uploadSync', err.message);
    });
  },
  's3.listBuckets' () {
    // debugger;
    var s3 = new AWS.S3(config);

    s3.listBuckets(function (err, data) {
      console.log(err, data);
    });

    // TODO: Add promise
  },
  's3.listObjects' () {
    // debugger;
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName
      // Delimiter: 'STRING_VALUE',
      // EncodingType: url,
      // Marker: 'STRING_VALUE',
      // MaxKeys: 'NUMBER_VALUE',
      // Prefix: 'STRING_VALUE',
      // RequestPayer: requester
    };
    var listObjectsPromise = s3.listObjects(params).promise();

    listObjectsPromise.then(function (data) {
    }).catch(function (err) {
      console.log(err);
      throw new Meteor.Error('s3.uploadSync', err.message);
    });

    return listObjectsPromise;
  },
  's3.listObjectsSync' (prefix) {
    // debugger;
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName,
      // Delimiter: 'STRING_VALUE',
      // EncodingType: url,
      // Marker: 'STRING_VALUE',
      // MaxKeys: 'NUMBER_VALUE',
      Prefix: prefix
      // RequestPayer: requester
    };
    let list = {};

    try {
      list = s3.listObjectsSync(params);
    } catch (err) {
      console.log(err);
      throw new Meteor.Error('s3.uploadSync', err.message);
    }

    return list;
  },
  's3.getObject' (fileName) {
    // debugger;
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName,
      Key: fileName
    };

    const response = s3.getObject(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
      } else {
        /*
                data = {
                 AcceptRanges: "bytes",
                 ContentLength: 3191,
                 ContentType: "image/jpeg",
                 ETag: "\"6805f2cfc46c0f04559748bb039d69ae\"",
                 LastModified: <Date Representation>,
                 Metadata: {
                 },
                 TagCount: 2,
                 VersionId: "null"
                }
                */
      }
    });

    return response;

    // TODO: Add promise
  },
  's3.getObjectSync' (fileName) {
    // debugger;
    var s3 = new AWS.S3(config);
    const params = {
      Bucket: Meteor.settings.private.s3.bucketName,
      Key: fileName
    };
    let object = {};

    try {
      object = s3.getObjectSync(params);
    } catch (err) {
      // debugger;
      console.log(err);
      throw new Meteor.Error('s3.uploadSync', err.message);
    }

    return object;
  }
});

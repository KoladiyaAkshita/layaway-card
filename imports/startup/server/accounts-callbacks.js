import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Profiles } from '../../startup/both/collections/profiles.js';

Accounts.onLogin(function () {
  console.log('Accounts.onLogin');

  const profile = Profiles.findOne({ userId: Meteor.userId() });

  if (profile) {
    Meteor.call('profiles.addUserKey', profile._id, profile);
  }
});

Accounts.onCreateUser(function (options, user) {
  console.log('Accounts.onCreateUser');

  let profile = null;

  // NOTE:
  // https://guide.meteor.com/accounts.html#adding-fields-on-registration
  // http://docs.meteor.com/api/accounts.html#requestpermissions
  if (user.services.facebook) {
    profile = {
      firstName: user.services.facebook.first_name,
      lastName: user.services.facebook.last_name,
      agreeToTermsAndConditions: true,
      userEmail: user.services.facebook.email,
      isActive: true,
      userId: user._id,
      createdBy: user._id,
      companyName: options.profile.companyName
    };
  } else if (options.profile) {
    profile = {
      firstName: options.profile.firstName,
      lastName: options.profile.lastName,
      agreeToTermsAndConditions: options.profile.agreeToTermsAndConditions === true ? true : null,
      userEmail: user.emails[0].address,
      isActive: true,
      userId: user._id,
      createdBy: user._id,
      companyName: options.profile.companyName,
      role: options.profile.role
    };
  }

  try {
    const adminProfile = Profiles.findOne({ userId: Meteor.userId() });

    if (user.services.facebook || options.profile) {
      console.log('Accounts.onCreateUser as new user');
      profile = Meteor.call('profiles.add', profile);
      console.log('User profile created: ', profile);

      return user;
    } else if (adminProfile && (adminProfile.role() === 'Admin' || adminProfile.role() === 'CompanyAdmin')) {
      console.log('Accounts.onCreateUser as Admin');

      return user;
    }
  } catch (error) {
    console.log('Accounts.onCreateUser error:', error);

    throw new Meteor.Error('account-creation-error', error.message);
  }
});

Accounts.validateLoginAttempt(function (info) {
  if (!info.allowed) {
    console.log('Login not allowed.');

    return false;
  }

  const profile = Profiles.findOne({ userId: info.user._id });

  if (profile && profile.isActive) {
    return true;
  }

  return false;
});

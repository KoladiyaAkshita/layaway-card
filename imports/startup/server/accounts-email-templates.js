import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Accounts.emailTemplates.siteName = Meteor.settings.public.applicationName;
Accounts.emailTemplates.from = Meteor.settings.private.email.from;
Accounts.emailTemplates.verifyEmail = {
  subject: function () {
    return Meteor.settings.private.email.verifyEmailSubject;
  },
  text: function () {
    return Meteor.settings.private.email.verifyEmailContent;
  }
};
/*
Accounts.emailTemplates.resetPassword.subject = function (user) {
    return "Message for " + user.profile.displayName;
};

Accounts.emailTemplates.resetPassword.text = function (user, url) {
    const signature = "Team";

    return "Dear " + user.profile.displayName + ",\n\n" +
        "Click the following link to set your new password:\n" +
        url + "\n\n" +
        "Please never forget it again!!!\n\n\n" +
        "Cheers,\n" +
        signature;
};
*/

import { Picker } from 'meteor/meteorhacks:picker';
import { Profiles } from '../both/collections/profiles.js';
import { Meteor } from 'meteor/meteor';

const bodyParser = require('body-parser');

Picker.middleware(bodyParser.json());
Picker.middleware(bodyParser.urlencoded({ extended: false }));
Picker.route('/webhooks/stripe', function (params, request, response) {
  const requestBody = request.body;
  const stripeResponse = requestBody.data.object;
  const profile = requestBody ? Profiles.findOne({ customerNumber: requestBody.customer }) : '';

  switch (requestBody.type) {
    case 'invoice.payment_failed':

      if (stripeResponse.paid === false) {
        PastDue(stripeResponse, profile);
      }

      break;

    case 'customer.subscription.deleted':
      SubscriptionDeleted(stripeResponse, profile);
      break;
            // case "charge.failed":
            //     var chargeFailed = stripeResponse.status;

            //     if (chargeFailed === "failed") {
            //         ChargeFailed(stripeResponse, profile);
            //     }

            //     response.statusCode = 200;
            //     response.end("Thank you Stripe\n");
            //     break;
  }

  response.statusCode = 200;
  response.end('Thank you Stripe\n');
}, { where: 'server' });

function PastDue (stripeResponse, profile) {
  const customerEmail = profile.userEmail;
  const adminEmail = customerEmail + 'has an unpaid invoice. Stripe subscription remains active, please set user to inactive.';
  const customerText = 'Hi,\n\nUnfortunately, your most recent payment for invoice# ' + stripeResponse.number + ' was unable to process.' +
        '\n\nPlease contact your finiancial institution or update current payment information' + '\n\n\nYour subscription will be canceled in 30 days due to non-payment.';

  Meteor.call('sendEmail', 'admin@themullingsgroup.com', 'customerservice@themullingsgroup.com', 'Account Subscription Alert', adminEmail);
  Meteor.call('sendEmail', customerEmail, 'customerservice@themullingsgroup.com', 'Account is past due', customerText);
}

function SubscriptionDeleted (stripeResponse, profile) {
  // Need to add call for user profile subscription changes.
  if (profile !== null && profile !== '' && typeof profile !== 'undefined') {
    const customerEmail = profile.userEmail;
    const canceledAdminText = customerEmail + 'subscription has been canceled. Either the customer requested the cancelation or it was done by customer service.\n\n' +
        'Please verify all invoices are paid for the customer.';
    const canceledCustomerText = 'Your account has been canceled. If you requested this cancelation please contact customerservice@themullingsgroup.com for any remaining data.\n\n' +
        "If you didn't request this cancelation please contact customerservice@themullingsgroup.com.\n\nThank you from all of us at The Mullings Group LLC";

    Meteor.call('sendEmail', customerEmail, 'customerservice@themullingsgroup.com', 'Sorry to see you go.', canceledCustomerText);
    Meteor.call('sendEmail', 'admin@themullingsgroup.com', 'customerservice@themullingsgroup.com', 'Customer subscription is canceled.', canceledAdminText);
  } else {
    const customerNotFoundText = 'A profile does not exist for the customer that is to be deleted. Please verify both Stripe dashboard and the users information.';
    Meteor.call('sendEmail', 'admin@themullingsgroup.com', 'customerservice@themullingsgroup.com', "Customer profile doesn't exist.", customerNotFoundText);
  }
}

// function ChargeFailed (stripeResponse, profile) {
//   profile = stripeResponse ? Profiles.findOne({ customerNumber: stripeResponse.customer }) : '';
//   const chargeFailedText = 'Hello,\n\nYour most recent payment was declined. Please contact your financial institution or enter a new card.';

//   if (profile !== '' && profile !== null && typeof profile !== 'undefined') {
//     Meteor.call('sendEmail', profile.userEmail, 'customerservice@themullingsgroup.com', 'Your card was declined', chargeFailedText);
//   } else {
//     Meteor.call('sendEmail', stripeResponse.receipt_email, 'customerservice@themullingsgroup.com', 'Your card was declined', chargeFailedText);
//   }
// }

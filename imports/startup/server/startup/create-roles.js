import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';

console.log('Creating roles records...');

for (const key in Meteor.settings.public.roles) {
  if (Object.prototype.hasOwnProperty.call(Meteor.settings.public.roles, key)) {
    const role = Meteor.settings.public.roles[key];

    Roles.createRole(role.value, { unlessExists: true });
  }
}

Roles.createRole('active', { unlessExists: true });
console.log('Roles records created.');

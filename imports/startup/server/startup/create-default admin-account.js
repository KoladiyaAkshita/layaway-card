import { Roles } from 'meteor/alanning:roles';
import { Profiles } from '../../both/collections/profiles.js';
import { Companies } from '../../both/collections/companies.js';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

if (Meteor.settings.private.defaultAdmin.enable) {
  createAdmin();
}

/**
 *
 */
function createAdmin () {
  console.log('Checking if default admin is account created...');
  const adminUser = Meteor.users.findOne({ 'emails.0.address': Meteor.settings.private.defaultAdmin.email });

  if (!adminUser) {
    console.log('Creating default admin account...');
    const adminProfile = {
      companyName: Meteor.settings.private.defaultAdmin.companyName,
      email: Meteor.settings.private.defaultAdmin.email,
      firstName: Meteor.settings.private.defaultAdmin.firstName,
      lastName: Meteor.settings.private.defaultAdmin.lastName,
      password: Meteor.settings.private.defaultAdmin.password,
      role: 'Admin'
    };
    const userId = Accounts.createUser(adminProfile);
    const profile = {
      agreeToTermsAndConditions: true,
      userEmail: Meteor.settings.private.defaultAdmin.email,
      firstName: Meteor.settings.private.defaultAdmin.firstName,
      lastName: Meteor.settings.private.defaultAdmin.lastName,
      isActive: true,
      userId: userId,
      createdBy: userId
    };
    let company = Companies.findOne({ name: Meteor.settings.private.defaultAdmin.companyName });
    let companyId = null;

    if (company) {
      companyId = company._id;
    } else {
      company = {
        name: Meteor.settings.private.defaultAdmin.companyName,
        createdBy: userId
      };
      companyId = Companies.insert(company);
    }

    Meteor.users.update({ 'emails.0.address': Meteor.settings.private.defaultAdmin.email }, { $set: { 'emails.0.verified': true } });

    profile.companyId = companyId;
    Profiles.insert(profile);
    Roles.addUsersToRoles(userId, ['Admin', 'active'], companyId);
    console.log('Default admin account created.');
  } else {
    console.log('Default admin account already created.');
  }
}

import { Meteor } from 'meteor/meteor';

Meteor.startup(function () {
  // process.env.MAIL_URL = `smtp://${Meteor.settings.private.email.sendGridUsername}:${Meteor.settings.private.email.sendGridPassword}@smtp.sendgrid.net:587`;
  process.env.MAIL_URL = `smtps:apikey:${Meteor.settings.private.email.sendGridApiKey}@smtp.sendgrid.net:465?tls.rejectUnauthorized=false&ignoreTLS=false&requireTLS=true&logger=false&debug=false`;
  process.env.SENDGRID_API_KEY = Meteor.settings.private.email.sendGridApiKey;
});

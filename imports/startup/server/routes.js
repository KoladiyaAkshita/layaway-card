/**
 * NOTE: http://creativenotice.com/2013/07/regular-expression-in-array-indexof/
 * Regular Expresion IndexOf for Arrays
 * This little addition to the Array prototype will iterate over array
 * and return the index of the first element which matches the provided
 * regular expresion.
 * Note: This will not match on objects.
 * @param  {RegEx}   rx The regular expression to test with. E.g. /-ba/gim
 * @return {Numeric} -1 means not found
 */
import { Picker } from 'meteor/meteorhacks:picker';

import CryptoJS from 'crypto-js';
import { AuthTokens } from '../both/collections/auth-tokens.js';

if (typeof Array.prototype.regExIndexOf === 'undefined') {
  Array.prototype.regExIndexOf = function (regEx) { // eslint-disable-line
    for (var i = 0; i < this.length; i++) {
      if (this[i].toString().match(regEx)) {
        return i;
      }
    }

    return -1;
  };
}

/**
 *
 */
Picker.route('/api/v1.0/example', function (params, request, response, next) {
  processRoute('/api/v1.0/kpi-results', params, request, response, next);
});

/**
 *
 */
var processRoute = function (route, params, request, response, next) {
  try {
    // console.log("request: ");
    // console.log(request);

    var authenticated = authenticate(request, params.query, response);

    if (authenticated) {
      if (request.method === 'GET') {
        var validatedHeaders = validateHeaders(request, params.query, response);

        if (validatedHeaders) {
          var validatedParams = validateParams(params, response);

          if (validatedParams) {
            var results = processRequest(request, params, route);

            respond(results, 200, response);
          }
        }
      } else {
        respondWithError('Method Not Allowed.', {}, params, 405, response);
      }
    }
  } catch (error) {
    respondWithError('An Error Occurred.', error, params, 500, response);
  }
};

/**
 *
 * @param {*} param
 */
const exists = function (param) {
  if ((typeof param === 'undefined' || param === null || param.trim() === '')) {
    return false;
  }

  return true;
};

/**
 * https://stackpointer.io/security/decode-http-basic-access-authentication/277/
 * @param {*} request
 * @param {*} params
 * @param {*} response
 */
const authenticate = function (request, params, response) {
  console.log('authenticating...');
  // console.log(request.rawHeaders);

  try {
    const indexOfAuthorizationString = request.rawHeaders.regExIndexOf(/(Authorization)/i);
    const authorizationString = indexOfAuthorizationString >= 0 ? request.rawHeaders[indexOfAuthorizationString + 1] : '';
    // console.log("authorizationString: " + authorizationString);

    if (!exists(authorizationString)) {
      respondWithError('Invalid API key.', {}, params, 401, response);

      return false;
    }

    const encryptedKeys = authorizationString.split(' ')[1];
    // console.log("encryptedKeys: " + encryptedKeys);
    const decryptedKeys = CryptoJS.enc.Base64.parse(encryptedKeys).toString(CryptoJS.enc.Utf8);
    // console.log("decryptedKeys: " + decryptedKeys);
    let publicKey = decryptedKeys.split(':')[0];
    // console.log("publicKey: " + publicKey);
    let privateKey = decryptedKeys.split(':')[1];
    // console.log("privateKey: " + privateKey);
    const authToken = AuthTokens.findOne({ publicKey: publicKey });
    // console.log(authToken);
    const encryptedPrivateKey = authToken ? CryptoJS.AES.decrypt(authToken.privateKey.toString(), authToken.createdBy) : '';
    // console.log("encryptedPrivateKey: " + encryptedPrivateKey);
    const decryptedPrivateKey = encryptedPrivateKey.toString(CryptoJS.enc.Utf8);
    // console.log("decryptedPrivateKey: " + decryptedPrivateKey);

    if (authToken && decryptedPrivateKey === privateKey) {
      privateKey = null;
      publicKey = null;
    } else {
      respondWithError('Invalid API key.', {}, params, 401, response);

      return false;
    }

    const indexOfApplicationName = request.rawHeaders.regExIndexOf(/(ApplicationName)/i);
    const applicationName = indexOfApplicationName >= 0 ? request.rawHeaders[indexOfApplicationName + 1] : '';

    // console.log("applicationName: " + applicationName);

    if (!exists(applicationName) || !authToken || authToken.applicationName !== applicationName) {
      respondWithError('Invalid application name.', {}, params, 401, response);

      return false;
    }

    // var indexOfHost = request.rawHeaders.regExIndexOf(/(Host)/i);
    // var host = indexOfHost >= 0 ? request.rawHeaders[indexOfHost + 1] : "";
    // console.log("host: " + host);

    // if (!exists(host) || !authToken || authToken.url !== host) {
    //     respondWithError("Invalid host.", {}, params, 401, response);

    //     return false;
    // }

    console.log('authenticated!');

    return true;
  } catch (error) {
    respondWithError('An Error Occurred. Not Authenticated.', error, params, 500, response);

    return false;
  }
};

/**
 *
 * @param {*} request
 * @param {*} params
 * @param {*} response
 */
var validateHeaders = function (request, params, response) {
  // var indexOfKey = request.rawHeaders.indexOf("key") >= 0 ? request.rawHeaders.indexOf("key") : request.rawHeaders.indexOf("Key");
  // var key = request.rawHeaders[indexOfKey + 1];
  // console.log("key: " + key);

  // if (!ifExists(key)) {
  //     console.log("invalid key!");
  //     error("Invalid key.", params, 500, response);

  //     return false;
  // }

  return true;
};

/**
 *
 * @param {*} param
 * @param {*} response
 */
var validateParams = function (params, response) {
  // var dbname = params.dbname;
  // var table = params.table;
  // var where = params.query.where;
  // var raw = params.query.raw;
  // var dataType = params.query.dataType;
  // var skipCount = params.query.skipCount;
  // var numberOfRows = params.query.numberOfRows;

  params.error = '';

  console.log('validating params...');

  // Validate query parameters
  /* params.error = exists(dbname) ? "" : "dbname is invalid.";

      if (params.error === "") {
          params.error = exists(table) ? "" : "table is invalid.";

          if (params.error === "") {
              if (exists(raw) && raw !== "false" && raw !== false && raw !== "true" && raw !== true) {
                  params.error = "raw is invalid, should be true or false (default is false).";
              } else if (exists(dataType) && dataType !== "JSON" && dataType !== "CSV") {
                  params.error = "dataType is invalid.";
              } else if (exists(skipCount) && !RegExp(/\d/g).test(skipCount)) {
                  params.error = "skipCount is invalid.";
              } else if (exists(numberOfRows) && !RegExp(/\d/g).test(numberOfRows)) {
                  params.error = "numberOfRows is invalid.";
              }
          }
      } */

  if (exists(params.error)) {
    respondWithError(params.error, {}, params, 400, response);

    return false;
  }

  console.log('params validated.');

  return true;
};

/**
 *
 * @param {*} request
 * @param {*} params
 * @param {*} route
 */
const processRequest = function (request, params, route) {
  // NOTE: Set defaults
  // if (!params.query.quantityPerPallet) {
  //     params.query.quantityPerPallet = Meteor.settings.private.api.defaultQuantityPerPallete;
  // }

  const indexOfAuthorizationString = request.rawHeaders.indexOf('authorization') >= 0 ? request.rawHeaders.indexOf('authorization') : request.rawHeaders.indexOf('Authorization');
  const authorizationString = indexOfAuthorizationString >= 0 ? request.rawHeaders[indexOfAuthorizationString + 1] : '';
  const encryptedKeys = authorizationString.split(' ')[1];
  const decryptedKeys = CryptoJS.enc.Base64.parse(encryptedKeys).toString(CryptoJS.enc.Utf8);
  let publicKey = decryptedKeys.split(':')[0];
  const authToken = AuthTokens.findOne({ publicKey: publicKey });

  publicKey = null;

  params.authTokenId = authToken._id;
  params.createdBy = authToken.createdBy;

  // TODO: Insert code here
  // const results = Meteor.call("db.read", params);
  const results = null;

  params.authTokenId = null;
  params.createdBy = null;

  return results;
};

/**
 *
 * @param {*} message
 * @param {*} error
 * @param {*} params
 * @param {*} statusCode
 * @param {*} response
 */
const respondWithError = function (message, error, params, statusCode, response) {
  console.log('API error occurred:');
  console.log(message);
  console.log(error);
  params.status = 'error';
  params.error = message;
  respond(params, statusCode, response);
};

/**
 *
 * @param {*} results
 * @param {*} response
 */
var respond = function (results, statusCode, response) {
  // TODO: Do we need to take pre-flight into considertion and implement OPTIONS

  // Enable cross origin requests for all endpoints
  // JsonRoutes.setResponseHeaders({
  //     "Cache-Control": "no-store",
  //     "Pragma": "no-cache",
  //     "Access-Control-Allow-Origin": "*",
  //     "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, OPTIONS",
  //     "Access-Control-Allow-Headers": "Content-Type, Authorization, X-Requested-With"
  // });
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  response.setHeader('Access-Control-Allow-Methods', 'GET');
  response.statusCode = statusCode;

  if (statusCode !== 200) {
    response.setHeader('Content-Type', 'text/html');
  } else {
    response.setHeader('Content-Type', 'application/json');
  }

  response.end(JSON.stringify(results));
};

// Import server startup through a single index entry point

import './startup';
import './accounts-callbacks.js';
import './accounts-email-templates.js';
import './email-methods.js';
// import './stripe-methods.js';
// import './stripe-webhooks.js';
// import './aws-methods.js';
import './routes.js';
// import './migrations'

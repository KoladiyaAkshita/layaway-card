import { $ } from 'meteor/jquery';
import { _ } from 'meteor/underscore';
import { Meteor } from 'meteor/meteor';
import { Profiles } from '../both/collections/profiles';

export const Utils = {
  phoneFormatter: {
    keypress: function (event, format) {
      const key = event.charCode || event.keyCode || 0;
      const phone = $(event.currentTarget);
      let skip = true;

      if (format === '-') {
        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {
          if (phone.val().length === 3) {
            phone.val(phone.val() + '-');
          }
          if (phone.val().length === 7) {
            phone.val(phone.val() + '-');
          }
          if (phone.val().length >= 12) {
            skip = false;
          }
        }
      } else if (format === '(') {
        if (phone.val().length === 0) {
          phone.val(phone.val() + '(');
        }
        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {
          if (phone.val().length === 4) {
            phone.val(phone.val() + ')');
          }
          if (phone.val().length === 5) {
            phone.val(phone.val() + ' ');
          }
          if (phone.val().length === 9) {
            phone.val(phone.val() + '-');
          }
          if (phone.val().length >= 14) {
            skip = false;
          }
        }
      }

      // Allow numeric (and tab, backspace, delete) keys only
      return (
        skip &&
        (key === 8 ||
          key === 9 ||
          key === 46 ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105))
      );
    },
    focus: function (event) {
      const phone = $(event.currentTarget);

      if (phone.val().length === 0) {
        phone.val('(');
      } else {
        const val = phone.val();
        phone.val('').val(val); // Ensure cursor remains at the end
      }
    },
    blur: function (event) {
      const phone = $(event.currentTarget);

      if (phone.val() === '(') {
        phone.val('');
      }
    }
  },
  determineProfileRoute: function () {
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const currentUserRole = profile ? profile.role() : '';
    let profileRoute = '/profiles';

    if (currentUserRole === 'Admin') {
      profileRoute = '/admin/profiles';
    } else if (currentUserRole === 'CompanyAdmin') {
      profileRoute = '/company-admin/profiles';
    }

    return profileRoute;
  },
  determineRoleRoute: function () {
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const currentUserRole = profile ? profile.role() : '';
    let profileRoute = '';

    if (currentUserRole === 'Admin') {
      profileRoute = '/admin';
    } else if (currentUserRole === 'CompanyAdmin') {
      profileRoute = '/company-admin';
    }

    return profileRoute;
  },
  validObject: function (object) {
    let isValid = false;
    let validCount = 0;

    for (const i in object) {
      if (typeof object[i] !== 'undefined' && object[i] !== null && object[i] !== '') {
        validCount += 1;
      }
    }

    if (validCount === _.size(object)) {
      isValid = true;
    }

    return isValid;
  }
};

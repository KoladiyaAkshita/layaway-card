import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Roles } from 'meteor/alanning:roles';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { AccountsTemplates } from 'meteor/useraccounts:core';
import { Profiles } from '../both/collections/profiles';

FlowRouter.wait(); // Pause FlowRouter until Roles information is ready

Tracker.autorun(function () {
  if (typeof Meteor.user() !== 'undefined' && Meteor.user() !== null && Meteor.user() !== '') {
    const userProfileSubscription = Meteor.subscribe('userProfile', Meteor.userId(), {
      onStop: function (error) {
        if (error) {
          // Bert.alert(error.reason, 'danger', 'fixed-top');
        }
      }
    });

    if (userProfileSubscription.ready() && Roles.subscription.ready() && !FlowRouter._initialized) {
      FlowRouter.initialize();
      console.log('Profile and Roles ready and FlowRouter initialized.');
    }
  } else if (Roles.subscription.ready() && !FlowRouter._initialized) {
    // If the roles subscription is ready, start routing
    // there are specific cases that this reruns, so we also check
    // that FlowRouter hasn't initialized already
    FlowRouter.initialize();
    console.log('Roles ready and FlowRouter initialized.');
  }
});

/** Un-authenticated user redirection */
FlowRouter.triggers.enter([function (context, redirect) {
  const current = FlowRouter.current();

  // console.log('Current route name: ' + current.route.name);
  window.scrollTo(0, 0);

  if (!Meteor.userId()) { // True if user not logged in
    const publicTemplates = Meteor.settings.public.publicTemplates;

    if (Meteor.settings.public.enableSignUp) {
      publicTemplates.push('atSignUp');
    }

    // True if on a screen that does not need authentication
    if (publicTemplates.indexOf(current.route.name) < 0) {
      FlowRouter.go('/');
    }
  }
}]);

/** Track screens that the user visits */
FlowRouter.triggers.exit([function (context, redirect) {
  var current = FlowRouter.current();

  // NOTE: Do not track upsert pages as clicking back after
  // list -> details -> upsert -> details will not have the desired effect
  if (current.path.toLowerCase().indexOf('upsert') < 0) {
    trackPreviousUrl(context, redirect);
  }
}]);

/**
 *
 * @param {*} context
 * @param {*} redirect
 */
function trackPreviousUrl (context, redirect) {
  // console.log('trackPreviousUrl: ' + FlowRouter.current().path);

  if (Meteor.userId()) { // True if user not logged in
    UserSession.set('previousUrl', FlowRouter.current().path);
  }
}

/** AccountTemplates routes */
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('verifyEmail');

if (Meteor.settings.public.enableSignUp && Meteor.settings.public.subscriptionLevel > 0) {
  AccountsTemplates.configureRoute('signUp');
  AccountsTemplates.configureRoute('enrollAccount');
}

/** Not found route handler */
FlowRouter.notFound = {
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'notFound' });
  }
};

/** App routes */

// Home
FlowRouter.route('/', {
  name: 'home',
  action: function () {
    if (Meteor.userId()) {
      BlazeLayout.render('MasterLayout', { yield: 'home' });
    } else {
      FlowRouter.go('/sign-in');
    }
  }
});

// Privacy Policy
FlowRouter.route('/privacy-policy', {
  name: 'privacyPolicy',
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'privacyPolicy' });
  }
});

FlowRouter.route('/terms-of-use', {
  name: 'termsOfUse',
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'termsOfUse' });
  }
});

FlowRouter.route('/thank-you', {
  name: 'thankYou',
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: "thankYou" });
  }
});

FlowRouter.route('/pricing', {
  name: 'pricing',
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'pricing' });
  }
});

//#region CompanyAdmin routes
let companyAdminRoutes = FlowRouter.group({
  prefix: '/company-admin',
  name: 'adminGroup',
  triggersEnter: [(context, redirect) => {
    const profile = Profiles.findOne({ userId: Meteor.userId() });

    if (profile && !Roles.userIsInRole(Meteor.userId(), 'Admin', profile.companyId) && !Roles.userIsInRole(Meteor.userId(), 'CompanyAdmin', profile.companyId)) {
      redirect('/page-not-found');
    }
  }],
});

// Route: /company-admin/companies/edit/:id
companyAdminRoutes.route("/companies/edit/:id", {
  name: "upsertCompany",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertCompany" });
  }
});

// Route: /company-admin/companies/:id
companyAdminRoutes.route("/companies/:id", {
  name: "company",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "company" });
  }
});
//#endregion

//#region Admin routes
const adminRoutes = FlowRouter.group({
  prefix: '/admin',
  name: 'adminGroup',
  triggersEnter: [(context, redirect) => {
    const profile = Profiles.findOne({ userId: Meteor.userId() });

    if (profile && !Roles.userIsInRole(Meteor.userId(), 'Admin', profile.companyId)) {
      redirect('/not-found');
    }
  }]
});

// Route: /admin/profiles
adminRoutes.route('/profiles', {
  name: 'profiles',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'profiles' });
  }
});

// Route: /admin/profiles/add
adminRoutes.route('/profiles/add', {
  name: 'upsertProfile',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'upsertProfile' });
  }
});

// Route: /admin/profiles/edit/:id
adminRoutes.route('/profiles/edit/:id', {
  name: 'upsertProfile',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'upsertProfile' });
  }
});

// Route: /admin/profiles/:id
adminRoutes.route('/profiles/:id', {
  name: 'profile',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'profile' });
  }
});

// Route: /admin/companies
adminRoutes.route('/companies', {
  name: 'companies',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'companies' });
  }
});

// Route: /admin/companies/add
adminRoutes.route('/companies/add', {
  name: 'upsertCompany',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'upsertCompany' });
  }
});

// Route: /admin/companies/edit/:id
adminRoutes.route('/companies/edit/:id', {
  name: 'upsertCompany',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'upsertCompany' });
  }
});

// Route: /admin/companies/:id
adminRoutes.route('/companies/:id', {
  name: 'company',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'company' });
  }
});

// Route: /admin/auth-tokens
adminRoutes.route('/auth-tokens', {
  name: 'authTokens',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'authTokens' });
  }
});

// Route: /admin/auth-tokens/add
adminRoutes.route('/auth-tokens/add', {
  name: 'upsertAuthToken',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'upsertAuthToken' });
  }
});

// Route: /admin/auth-tokens/edit/:id
adminRoutes.route('/auth-tokens/edit/:id', {
  name: 'upsertAuthToken',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'upsertAuthToken' });
  }
});

// Route: /admin/auth-tokens/:id
adminRoutes.route('/auth-tokens/:id', {
  name: 'authToken',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'authToken' });
  }
});

// #endregion

FlowRouter.route('/profile', {
  name: 'profile',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render('MasterLayout', { yield: 'profile' });
  }
});

FlowRouter.route("/cards/:id", {
  name: "card",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "card" });
  }
});

FlowRouter.route("/layaway-items", {
  name: "layawayItems",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "layawayItems" });
  }
});

FlowRouter.route("/layaway-items/add", {
  name: "upsertLayawayItem",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertLayawayItem" });
  }
});

FlowRouter.route("/layaway-items/edit/:id", {
  name: "upsertLayawayItem",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertLayawayItem" });
  }
});

FlowRouter.route("/layaway-items/:id", {
  name: "layawayItem",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "layawayItem" });
  }
});

FlowRouter.route("/items", {
  name: "items",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "items" });
  }
});

FlowRouter.route("/items/add", {
  name: "upsertItem",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertItem" });
  }
});

FlowRouter.route("/items/edit/:id", {
  name: "upsertItem",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertItem" });
  }
});

FlowRouter.route("/items/:id", {
  name: "item",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "item" });
  }
});

FlowRouter.route("/payment-plans", {
  name: "paymentPlans",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "paymentPlans" });
  }
});

FlowRouter.route("/payment-plans/add", {
  name: "upsertPaymentPlan",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertPaymentPlan" });
  }
});

FlowRouter.route("/payment-plans/edit/:id", {
  name: "upsertPaymentPlan",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertPaymentPlan" });
  }
});

FlowRouter.route("/payment-plans/:id", {
  name: "paymentPlan",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "paymentPlan" });
  }
});

FlowRouter.route("/payments", {
  name: "payments",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "payments" });
  }
});

FlowRouter.route("/payments/add", {
  name: "upsertPayment",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertPayment" });
  }
});

FlowRouter.route("/payments/edit/:id", {
  name: "upsertPayment",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "upsertPayment" });
  }
});

FlowRouter.route("/payments/:id", {
  name: "payment",
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function () {
    BlazeLayout.render("MasterLayout", { yield: "payment" });
  }
});


// This file is used to import all the templates into the project.
// Add new templates to the project by importing them here.
import '../../ui/layouts/master-layout/master-layout.js';

import '../../ui/pages/not-found/not-found.js';

import '../../ui/pages/home/home.js';

import '../../ui/pages/privacy-policy/privacy-policy.js';

import '../../ui/pages/terms-of-use/terms-of-use.js';

import '../../ui/pages/thank-you/thank-you.js';

import '../../ui/pages/profiles/profiles.js';

import '../../ui/pages/profiles/upsert-profile/upsert-profile.js';

import '../../ui/pages/profiles/profile/profile.js';

import '../../ui/pages/companies/companies.js';

import '../../ui/pages/companies/upsert-company/upsert-company.js';

import '../../ui/pages/companies/company/company.js';

import '../../ui/pages/auth-tokens/auth-tokens.js';

import '../../ui/pages/auth-tokens/upsert-auth-token/upsert-auth-token.js';

import '../../ui/pages/auth-tokens/auth-token/auth-token.js';
import '../../ui/pages/cards/cards.js';

import '../../ui/pages/cards/upsert-card/upsert-card.js';

import '../../ui/pages/cards/card/card.js';

import '../../ui/pages/layaway-items/layaway-items.js';

import '../../ui/pages/layaway-items/upsert-layaway-item/upsert-layaway-item.js';

import '../../ui/pages/layaway-items/layaway-item/layaway-item.js';

import '../../ui/pages/items/items.js';

import '../../ui/pages/items/upsert-item/upsert-item.js';

import '../../ui/pages/items/item/item.js';

import '../../ui/pages/payment-plans/payment-plans.js';

import '../../ui/pages/payment-plans/upsert-payment-plan/upsert-payment-plan.js';

import '../../ui/pages/payment-plans/payment-plan/payment-plan.js';

import '../../ui/pages/payments/payments.js';

import '../../ui/pages/payments/upsert-payment/upsert-payment.js';

import '../../ui/pages/payments/payment/payment.js';


import { Template } from 'meteor/templating';
import { moment } from 'meteor/momentjs:moment';
import { Utils } from '../client/utils';
import { $ } from 'meteor/jquery';
import { Meteor } from 'meteor/meteor';

// http://stackoverflow.com/questions/22087907/how-to-format-date-in-meteor-template
Template.registerHelper('formatDate', function (dateTime) {
  if (dateTime) {
    return moment(dateTime).format('YYYY-MM-DD');
  }
});

Template.registerHelper('formatDateTime', function (dateTime) {
  if (dateTime) {
    return moment(dateTime).format('MM/DD/YYYY hh:mm A');
  }
});

Template.registerHelper('prettyPrintDate', function (dateTime) {
  if (dateTime) {
    return moment(dateTime).format('MM/DD/YYYY');
  }
});

Template.registerHelper('prettyPrintDateTime', function (dateTime) {
  if (dateTime) {
    return moment(dateTime).format('MM/DD/YYYY h:mm A');
  }
});

Template.registerHelper('prettyPrintJson', function (object) {
  if (object) {
    return JSON.stringify(JSON.parse(object), null, 2);
  }
});

Template.registerHelper('printObject', function (object) {
  if (object) {
    return JSON.stringify(object);
  }
});

Template.registerHelper('formatCurrency',
  $.fn.digits = function (value) {
    if (value) {
      return formatInput(value);
    }
  }
);

/**
 *
 * @param {*} value
 */
var formatInput = function (value) {
  if (value === 0) {
    return '$0.00';
  }

  const toks = value.toFixed(2).replace('-', '').split('.');

  if (value && value.toString().split('.').length > 1) {
    toks = value.toString().toFixed(2).replace('-', '').split('.');
  } else {
    toks = value.toString().replace('-', '').split('.');

    if (!toks[1]) {
      toks[1] = '00';
    }
  }

  var display = '$' + $.map(toks[0].split('').reverse(), function (elm, i) {
    return [(i % 3 === 0 && i > 0 ? ',' : ''), elm];
  }).reverse().join('') + '.' + toks[1];

  return value < 0 ? '(' + display + ')' : display;
};

Template.registerHelper('formatPhoneNumber', function (phoneNumber) {
  const separator = '-';
  let formattedNumber = '';

  if (phoneNumber) {
    formattedNumber = phoneNumber.replace(/[^\d]/g, '')
      .replace(/(\d{3})(\d{3})(\d{4})/, '$1' + separator + '$2' + separator + '$3');
  }

  return formattedNumber;
});

Template.registerHelper('concat', function () {
  if (arguments.length > 0) {
    return Array.prototype.slice.call(arguments, 0, -1).join('');
  }
});

Template.registerHelper('publicSettings', function (field) {
  return Meteor.settings.public[field];
});

Template.registerHelper('profileRoute', function () {
  return Utils.determineProfileRoute();
});

Template.registerHelper('roleRoute', function () {
  return Utils.determineRoleRoute();
});

import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

/**
 * NOTE: Wait on roles to intialise so we can check if user is in proper role
 * https://tzapu.com/meteor-alannings-roles-flow-router-not-checking-roles-routes/
 */
// FlowRouter.wait();

// Tracker.autorun(() => {
//     if (Roles.subscription.ready() && !FlowRouter._initialized) {
//         FlowRouter.initialize()
//     }
// });

/**
 * Use template subscription to get the current user's Profile
 */
Meteor.startup(function () {
  Tracker.autorun(function () {
    if (typeof Meteor.user() !== 'undefined' && Meteor.user() !== null && Meteor.user() !== '') {
      Meteor.subscribe('userProfile', Meteor.userId(), {
        onStop: function (error) {
          console.log('subscribe profile onStop @ Meteor.startup.Tracker.autorun', error);
        }
      });
    }
  });
});

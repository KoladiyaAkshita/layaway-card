// import { Bert } from 'meteor/themeteorchef:bert';
import { Meteor } from 'meteor/meteor';

Meteor.Spinner.options = {
  /*
    lines: 13, // The number of lines to draw
    length: 10, // The length of each line
    width: 5, // The line thickness
    radius: 15, // The radius of the inner circle
    corners: 0.7, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#fff', // #rgb or #rrggbb
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: true, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    */
  top: '30%', // Top position relative to parent in px
  left: '50%' // Left position relative to parent in px
};

// Bert.defaults = {
//     //animated: false,
//     // Accepts: true or false.
//     // animationSpeed: 300,
//     // Accepts: integer value in milliseconds.
//     // Note: this value needs to match the speed of the CSS transition-duration
//     // property on the .bert-alert.animated class. If it doesn't, Bert will freak out.
//     //autoHide: false,
//     // Accepts: true or false.
//     //dismissable: true,
//     // Accepts: true or false.
//     hideDelay: 7000
//     // Accepts: integer value in milliseconds.
//     //style: "fixed-top",
//     // Accepts: fixed-top, fixed-bottom, growl-top-left, growl-top-right,
//     // growl-bottom-left, growl-bottom-right.
//     //type: "default"
//     // Accepts: default, success, info, warning, danger.
// };

import { FlowRouter } from 'meteor/kadira:flow-router';
import { Roles } from 'meteor/alanning:roles';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from './../collections/profiles.js';
import { Meteor } from 'meteor/meteor';
import { AccountsTemplates } from 'meteor/useraccounts:core';

// AccountsTemplates Options
// NOTE: https://github.com/meteor-useraccounts/core/blob/master/Guide.md
// NOTE: https://github.com/meteor-useraccounts/flow-routing
/*
AccountsTemplates.configure({
    // Behavior
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: true,
    sendVerificationEmail: false,
    lowercaseUsername: false,
    focusFirstInput: true,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: false,
    showLabels: true,
    showPlaceholders: true,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    termsUrl: 'terms-of-use',

    // Redirects
    homeRoutePath: '/home',
    redirectTimeout: 4000,

    // Hooks
    onLogoutHook: myLogoutFunc,
    onSubmitHook: mySubmitFunc,
    preSignUpHook: myPreSubmitFunc,
    postSignUpHook: myPostSubmitFunc,

    // Texts
    texts: {
      button: {
          changePwd: "Password Text",
          enrollAccount: "Enroll Text",
          forgotPwd: "Forgot Pwd Text",
          resetPwd: "Reset Pwd Text",
          signIn: "Sign In Text",
          signUp: "Sign Up Text"
      },
      title: {
        changePwd: "Password Title",
        enrollAccount: "Enroll Title",
        forgotPwd: "Forgot Pwd Title",
        resetPwd: "Reset Pwd Title",
        signIn: "Sign In Title",
        signUp: "Sign Up Title",
        verifyEmail: "Verify Email Title"
      },
      info: {
        emailSent: "info.emailSent",
        emailVerified: "info.emailVerified",
        pwdChanged: "info.passwordChanged",
        pwdReset: "info.passwordReset",
        pwdSet: "info.passwordReset",
        signUpVerifyEmail: "Successful Registration! Please check your email and follow the instructions.",
        verificationEmailSent: "A new email has been sent to you. If the email doesn't show up in your inbox, be sure to check your spam folder.",
      },
      inputIcons: {
        isValidating: "fa fa-spinner fa-spin",
        hasSuccess: "fa fa-check",
        hasError: "fa fa-times",
      },
      errors: {
        accountsCreationDisabled: "Client side accounts creation is disabled!!!",
        cannotRemoveService: "Cannot remove the only active service!",
        captchaVerification: "Captcha verification failed!",
        loginForbidden: "error.accounts.Login forbidden",
        mustBeLoggedIn: "error.accounts.Must be logged in",
        pwdMismatch: "error.pwdsDontMatch",
        validationErrors: "Validation Errors",
        verifyEmailFirst: "Please verify your email first. Check the email and follow the link!",
      },
      navSignIn: "signIn",
      navSignOut: "signOut",
      optionalField: "optional",
      pwdLink_pre: "",
      pwdLink_link: "forgotPassword",
      pwdLink_suff: "",
      resendVerificationEmailLink_pre: "Verification email lost?",
      resendVerificationEmailLink_link: "Send again",
      resendVerificationEmailLink_suff: "",
      sep: "OR",
      signInLink_pre: "ifYouAlreadyHaveAnAccount",
      signInLink_link: "signin",
      signInLink_suff: "",
      signUpLink_pre: "dontHaveAnAccount",
      signUpLink_link: "signUp",
      signUpLink_suff: "",
      socialAdd: "add",
      socialConfigure: "configure",
      socialIcons: {
          google: "myGoogleIcon",
          "meteor-developer": "fa fa-rocket",
      },
      socialRemove: "remove",
      socialSignIn: "signIn",
      socialSignUp: "signUp",
      socialWith: "with",
      termsPreamble: "clickAgree",
      termsPrivacy: "privacyPolicy",
      termsAnd: "and",
      termsTerms: "terms"
    }
});
*/
AccountsTemplates.configure({
  // Behavior
  defaultLayout: 'MasterLayout',
  defaultLayoutRegions: {},
  defaultContentRegion: 'yield',
  overrideLoginErrors: true,
  enablePasswordChange: true,
  sendVerificationEmail: false,
  enforceEmailVerification: false,
  // confirmPassword: true,
  // continuousValidation: false,
  // displayFormLabels: true,
  forbidClientAccountCreation: !Meteor.settings.public.enableSignUp && Meteor.settings.public.subscriptionLevel > 0,
  hideSignUpLink: !Meteor.settings.public.enableSignUp,
  // formValidationFeedback: true,

  // Appearance
  // showAddRemoveServices: false,
  showForgotPasswordLink: true,
  // showLabels: true,
  showPlaceholders: true,
  showResendVerificationEmailLink: false,

  // Client-side Validation
  negativeValidation: true,
  positiveValidation: true,
  negativeFeedback: false,
  positiveFeedback: true,
  showValidating: true,

  // Privacy Policy and Terms of Use
  privacyUrl: 'privacy-policy',
  termsUrl: 'terms-of-use',

  // Hooks
  /**
   *
   */
  onSubmitHook: function (error) {
    console.log('onSubmitHook');

    if (error) {
      swal('Error', error.reason, 'error');
    } else {
      FlowRouter.go('/');
    }
  },
  /**
   *
   */
  preSignUpHook: function (password, info) {
    console.log('preSignUpHook: ', info);
  },
  /**
   *
   */
  postSignUpHook: function (userId, info) {
    console.log('postSignUpHook: ', info);

    const profile = Profiles.findOne({ userId: userId });

    // NOTE: This has to be done here instead of Accounts.onCreateUser or profiles.add
    // NOTE: "active" role should be added second
    console.log(`Adding user to roles active & ${info.profile.role}...`);
    Roles.createRole(info.profile.role, { unlessExists: true });
    Roles.createRole('active', { unlessExists: true });
    Roles.addUsersToRoles(profile.userId, [info.profile.role, 'active'], profile.companyId);
    console.log('User added to roles.');
  },

  // Texts
  texts: {
    button: {
      // changePwd: "Password Text",
      // enrollAccount: "Enroll Text",
      // forgotPwd: "Forgot Pwd Text",
      // resetPwd: "Reset Pwd Text",
      signIn: 'Login',
      signUp: 'Sign Up'
    },
    title: {
      // changePwd: "Password Title",
      // enrollAccount: "Enroll Title",
      // forgotPwd: "Forgot Pwd Title",
      // resetPwd: "Reset Pwd Title",
      signIn: 'Login',
      signUp: 'Sign Up'
      // verifyEmail: "Verify Email Title"
    },
    errors: {
      // accountsCreationDisabled: "Client side accounts creation is disabled!!!",
      // cannotRemoveService: "Cannot remove the only active service!",
      // captchaVerification: "Captcha verification failed!",
      loginForbidden: 'Login failed.',
      // mustBeLoggedIn: "error.accounts.Must be logged in",
      // pwdMismatch: "error.pwdsDontMatch",
      // validationErrors: "Validation Errors",
      verifyEmailFirst: 'Please verify your email first. Check your email and follow the link'
    },
    resendVerificationEmailLink_pre: '',
    resendVerificationEmailLink_link: 'Resend verification email',
    resendVerificationEmailLink_suff: '',
    signInLink_pre: 'ifYouAlreadyHaveAnAccount',
    signInLink_link: 'Sign in',
    // signInLink_suff: "",
    signUpLink_pre: 'dontHaveAnAccount',
    signUpLink_link: 'Sign Up'
    // signUpLink_suff: "up stuff",
    // termsPreamble: "clickAgree",
    // termsPrivacy: "privacyPolicy",
    // termsAnd: "and",
    // termsTerms: "terms"
  }
});

AccountsTemplates.removeField('password');
AccountsTemplates.addField({
  _id: 'password',
  type: 'password',
  required: true,
  minLength: 6,
  re: /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/, // eslint-disable-line
  errStr: 'At least 1 digit, 1 lower-case, 1 upper-case and 1 special character.'
});

AccountsTemplates.addField({
  _id: 'firstName',
  type: 'text',
  displayName: 'First Name',
  required: true
});

AccountsTemplates.addField({
  _id: 'lastName',
  type: 'text',
  displayName: 'Last Name',
  required: true
});

if (Meteor.settings.public.subscriptionLevel > 0) {
  AccountsTemplates.addField({
    _id: 'companyName',
    type: 'text',
    displayName: 'Company Name',
    required: true
  });
}

const select = [];

for (let index = 0; index < Meteor.settings.public.roles.length; index++) {
  const role = Meteor.settings.public.roles[index];

  if ((role.value === 'Admin' && !Meteor.settings.public.enableAdminSignUp) || !role.signUpRole) {
    continue;
  }

  select.push({
    text: role.text,
    value: role.value
  });
}

AccountsTemplates.addField({
  _id: 'role',
  type: 'radio',
  displayName: 'Account Type',
  required: true,
  select: select
});

AccountsTemplates.addField({
  _id: 'agreeToTermsAndConditions',
  type: 'checkbox',
  displayName: 'Agree to Terms and Conditions',
  required: true
});

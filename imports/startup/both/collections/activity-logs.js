import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Meteor } from 'meteor/meteor';
import { Profiles } from './profiles';

export const ActivityLogs = new Mongo.Collection('ActivityLogs');
export const ActivityLogsSchema = new SimpleSchema({
  collectionName: {
    type: String,
    optional: true
  },
  recordId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  action: {
    type: String,
    optional: true
  },
  key: {
    type: String,
    optional: true
  },
  value: {
    type: String,
    optional: true
  },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  }
});

ActivityLogs.attachSchema(ActivityLogsSchema);
ActivityLogs.helpers({
  createdByName: function () {
    const profile = Profiles.findOne({ userId: this.createdBy });

    if (profile) {
      return `${profile.firstName} ${profile.lastName}`;
    } else {
      return '';
    }
  },
  shortCreatedDate: function () {
    const date = new Date(this.createdDate);

    return `${date.toLocaleDateString()} at ${date.toLocaleTimeString()}`;
  }
});

Meteor.methods({
  'activityLogs.add' (activityLog) {
    activityLog.createdBy = this.userId;
    ActivityLogsSchema.clean(activityLog);

    return ActivityLogs.insert(activityLog);
  }
});

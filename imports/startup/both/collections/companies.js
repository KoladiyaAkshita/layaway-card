import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Profiles } from './profiles.js';

const collectionName = 'Companies';

export const Companies = new Mongo.Collection('Companies');
export const CompaniesSchema = new SimpleSchema({
  name: {
    type: String,
    max: 50,
    min: 3
    // regEx: /^(?=.{1,40}$)[a-zA-Z]+(?:[-' ][a-zA-Z]+)*$/
  },
  phone: {
    type: String,
    optional: true,
    regEx: SimpleSchema.RegEx.Phone
  },
  website: {
    type: String,
    optional: true,
    regEx: SimpleSchema.RegEx.Url
  },
  address1: {
    type: String,
    max: 50,
    optional: true
  },
  address2: {
    type: String,
    max: 50,
    optional: true
  },
  city: {
    type: String,
    max: 50,
    optional: true
  },
  state: {
    type: String,
    regEx: /^A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]$/,
    optional: true
  },
  zip: {
    type: String,
    optional: true
  },
  isDeleted: {
    type: Boolean,
    defaultValue: false
  },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
});

Companies.attachSchema(CompaniesSchema);
Meteor.methods({
  'companies.add' (company) {
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const role = profile ? profile.role() : '';

    if (this.userId && role !== 'Admin' && role !== 'CompanyAdmin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    }

    company.createdBy = this.userId;
    CompaniesSchema.clean(company);

    return Companies.insert(company);
  },
  'companies.edit' (id, company) {
    check(id, String);
    company.lastModifiedBy = this.userId;
    CompaniesSchema.clean(company);

    const updatedCompany = Companies.findOne({ _id: id });
    const activity = {
      collectionName: collectionName,
      recordId: id,
      action: 'EDITTED',
      value: JSON.stringify(updatedCompany),
      createdBy: this.userId
    };
    Meteor.call('activityLogs.add', activity);
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const role = profile ? profile.role() : '';

    if (this.userId && updatedCompany.createdBy !== this.userId && role !== 'Admin' && role !== 'CompanyAdmin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedCompany._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }

    return Companies.update({ _id: id }, {
      $set: {
        ...company,
        lastModifiedBy: company.lastModifiedBy
      }
    });
  },
  'companies.delete' (id) {
    check(id, String);

    const company = Companies.findOne({ _id: id });
    const activity = {
      collectionName: collectionName,
      recordId: id,
      action: 'DELETED',
      value: JSON.stringify(company),
      createdBy: this.userId
    };
    Meteor.call('activityLogs.add', activity);

    if (this.userId && company.createdBy !== this.userId) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (company._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }

    try {
      Companies.update({ _id: id }, {
        $set: {
          isDeleted: true,
          lastModifiedBy: this.userId
        }
      });
      return 'Company successfully deleted.';
    } catch (error) {
      throw new Meteor.Error('delete', 'Failed to delete company.');
    }
  }
});

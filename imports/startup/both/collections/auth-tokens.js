import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import CryptoJS from 'crypto-js';
import { Profiles } from './profiles.js';

export const AuthTokens = new Mongo.Collection('AuthTokens');
export const AuthTokensSchema = new SimpleSchema({
  applicationName: {
    type: String,
    max: 50
  },
  // url: {
  //   type: String
  // },
  privateKey: {
    type: String,
    regEx: /([A-Z+])\w+/
  },
  publicKey: {
    type: String,
    regEx: /([A-Z+])\w+/
  },
  companyId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
}, {
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true
  }
});

AuthTokens.attachSchema(AuthTokensSchema);

Meteor.methods({
  'authTokens.add' (authToken) {
    const profile = Profiles.findOne({ createdBy: Meteor.userId() });
    const role = profile.role();

    if (!Meteor.userId() && role !== 'Admin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    }

    check(authToken.publicKey, String);
    check(authToken.privateKey, String);

    authToken.companyId = profile ? profile.companyId : null;
    authToken.createdBy = Meteor.userId();
    AuthTokensSchema.clean(authToken);

    const privateDecrypt = CryptoJS.AES.decrypt(authToken.privateKey, profile.userKey).toString(CryptoJS.enc.Utf8);
    const privateEncrypt = CryptoJS.AES.encrypt(privateDecrypt, Meteor.userId());

    authToken.privateKey = privateEncrypt.toString();

    return AuthTokens.insert(authToken);
  },
  'authTokens.edit' (id, authToken) {
    check(id, String);
    AuthTokensSchema.clean(authToken);

    const updatedAuthToken = AuthTokens.findOne({ _id: id });
    const profile = Profiles.findOne({ createdBy: Meteor.userId() });
    const role = profile.role();

    if (!Meteor.userId() && role !== 'Admin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedAuthToken._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }

    return AuthTokens.update({ _id: id }, {
      $set: {
        applicationName: authToken.applicationName,
        // url: authToken.url,
        lastModifiedBy: Meteor.userId()
      }
    });
  },
  'authTokens.delete' (id) {
    check(id, String);

    const authToken = AuthTokens.findOne({ _id: id });
    const profile = Profiles.findOne({ createdBy: Meteor.userId() });
    const role = profile.role();

    if (!Meteor.userId() && role !== 'Admin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (authToken._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }

    return AuthTokens.remove({ _id: id });
  }
});

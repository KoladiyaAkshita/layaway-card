import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { _ } from 'meteor/underscore';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { Random } from 'meteor/random';
import { Companies } from './companies';

const collectionName = 'Profiles';

export const Profiles = new Mongo.Collection(collectionName);
export const ProfilesSchema = new SimpleSchema({
  firstName: {
    type: String,
    max: 50,
    min: 3
    // regEx: /^(?=.{1,40}$)[a-zA-Z]+(?:[-' ][a-zA-Z]+)*$/
  },
  lastName: {
    type: String,
    max: 50,
    min: 1
    // regEx: /^(?=.{1,40}$)[a-zA-Z]+(?:[-' ][a-zA-Z]+)*$/
  },
  address1: {
    type: String,
    optional: true
  },
  address2: {
    type: String,
    optional: true
  },
  city: {
    type: String,
    optional: true
  },
  state: {
    type: String,
    regEx: /^A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]$/,
    optional: true
  },
  zip: {
    type: String,
    regEx: /^\d{5}$/,
    optional: true
  },
  phone: {
    type: String,
    max: 12,
    min: 10,
    regEx: SimpleSchema.RegEx.Phone,
    optional: true
  },
  userEmail: {
    type: String,
    max: 50,
    regEx: SimpleSchema.RegEx.Email
  },
  companyId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  userKey: {
    type: String,
    regEx: /([A-Z+])\w+/,
    optional: true
  },
  agreeToTermsAndConditions: {
    type: Boolean,
    defaultValue: false
  },
  isActive: {
    type: Boolean
  },
  isDeleted: {
    type: Boolean,
    defaultValue: false
  },
  userId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    },
    optional: true
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      return new Date();
    }
  }
});

Profiles.helpers({
  company: function () {
    return Companies.findOne({ _id: this.companyId });
  },
  // NOTE: User will only have 2 roles per group (company), one of which is the active role.
  role: function () {
    let role = '';

    if (Meteor.user()) {
      const currentUserProfile = Profiles.findOne({ userId: Meteor.userId() });
      const currentUserRoles = _.without(Roles.getRolesForUser(Meteor.userId(), currentUserProfile.companyId), 'active');
      const currentUserRole = currentUserRoles.length > 0 ? currentUserRoles[0] : '';
      let roles = [];

      if (Meteor.userId() === this.userId) {
        role = currentUserRole;
      } else if (currentUserRole === 'Admin') {
        roles = _.without(Roles.getRolesForUser(this.userId, this.companyId), 'active');
        role = roles.length > 0 ? roles[0] : '';
      } else if (currentUserRole === 'CompanyAdmin') {
        roles = _.without(Roles.getRolesForUser(this.userId, currentUserProfile.companyId), 'active');
        role = roles.length > 0 ? roles[0] : '';
      }
    }

    return role;
  }
});
Profiles.attachSchema(ProfilesSchema);
Meteor.methods({
  'profiles.add' (profile) {
    if (Meteor.isServer) {
      console.log('profiles.add');

      const user = Accounts.findUserByEmail(profile.userEmail);

      if (Meteor.user() === null && !user) {
        console.log('Creating company record...');
        const company = {
          name: profile.companyName,
          createdBy: profile.userId
        };
        const companyId = Companies.insert(company);

        console.log('Creating user profile record...');
        profile.companyId = companyId;
        ProfilesSchema.clean(profile);
        ProfilesSchema.validate(profile);
        console.log('New profile validated.');
        profile._id = Profiles.insert(profile);
        console.log('New profile created.');

        return profile;
      } else {
        console.log('profiles.add - invalid operation.');

        throw new Meteor.Error('invalid-operation', 'profiles.add - invalid operation.');
      }
    }
  },
  'profiles.addAsCompanyAdmin' (profile) {
    const companyAdminProfile = Profiles.findOne({ userId: this.userId });

    if (companyAdminProfile.role() !== 'CompanyAdmin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    }

    if (Meteor.isServer) {
      profile.createdBy = this.userId;
      profile.agreeToTermsAndConditions = true;
      profile.isActive = true;

      const user = Accounts.findUserByEmail(profile.userEmail);
      let profileId = null;

      if (!user) {
        profile.userId = Accounts.createUser({ email: profile.userEmail });
        console.log('New account created.');
        Roles.addUsersToRoles(profile.userId, [profile.role, 'active'], profile.companyId);
        console.log('New user roles added.');
        ProfilesSchema.clean(profile);
        ProfilesSchema.validate(profile);
        console.log('New profile validated.');
        profileId = Profiles.insert(profile);
        console.log('New profile created.');
        Accounts.sendEnrollmentEmail(profile.userId);
      } else {
        const existingProfile = Profiles.findOne({ userEmail: profile.userEmail });

        // NOTE: User can have an account for another company
        if (profile.companyId == existingProfile.companyId) {
          throw new Meteor.Error('account-already-exists', 'An account already exists with that email.');
        } else {
          Roles.addUsersToRoles(profile.userId, [profile.role, 'active'], profile.companyId);
          console.log('Updated existing profile.');
        }
      }

      companyAdminProfile.userCount += 1;
      Profiles.update({ _id: companyAdminProfile._id }, {
        $set: {
          userCount: companyAdminProfile.userCount
        }
      });
      console.log('Admin user count increased.');

      return profileId;
    }
  },
  'profiles.addAsAdmin' (profile) {
    if (Meteor.isServer) {
      const adminProfile = Profiles.findOne({ userId: Meteor.userId() });

      if (adminProfile.role() !== 'Admin') {
        throw new Meteor.Error('not-authorized', 'You are not authorized.');
      }

      const user = Accounts.findUserByEmail(profile.userEmail);
      let profileId = null;
      const profileRole = profile.role;

      if (user) {
        throw new Meteor.Error(
          'account-already-exists',
          'An account already exists with that email.'
        );
      }

      try {
        profile.userId = Accounts.createUser({ email: profile.userEmail });
        console.log('New account created.');
        profile.isActive = true;
        profile.agreeToTermsAndConditions = true;
        profile.createdBy = this.userId;

        ProfilesSchema.clean(profile);
        ProfilesSchema.validate(profile);
        console.log('New profile validated.');
        profileId = Profiles.insert(profile);
        console.log('New profile created.');
        Roles.addUsersToRoles(
          profile.userId,
          [profileRole, 'active'],
          profile.companyId
        );
        console.log('New user roles added.');

        if (Meteor.settings.private.email.sendEnrollmentEmail) {
          Accounts.sendEnrollmentEmail(profile.userId);
        }
      } catch (err) {
        // debugger;
        // Rollback user creation
        if (profile.userId) {
          Meteor.users.remove({ _id: profile.userId });
        }

        // Rollback Profile creation
        if (profileId) {
          Profiles.remove({ _id: profileId });
        }

        throw new Meteor.Error('account-create-error', err.message);
      }

      return profileId;
    }
  },
  'profiles.edit' (id, profile) {
    check(id, String);

    const existingProfile = Profiles.findOne({ _id: id });
    const activity = {
      collectionName: collectionName,
      recordId: id,
      action: 'EDITTED',
      value: JSON.stringify(existingProfile),
      createdBy: this.userId
    };
    Meteor.call('activityLogs.add', activity);

    const adminProfile = Profiles.findOne({ userId: Meteor.userId() });
    const currentUserIsAdmin = adminProfile.role() === 'Admin';
    const currentUserIsCompanyAdmin = adminProfile.role() === 'CompanyAdmin';

    if (
      (existingProfile.userId !== Meteor.userId() &&
        !currentUserIsAdmin &&
        !currentUserIsCompanyAdmin) ||
      (currentUserIsCompanyAdmin &&
        adminProfile.companyId !== existingProfile.companyId)
    ) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (existingProfile._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }

    if (
      !currentUserIsAdmin &&
      existingProfile.role() !== 'Admin' &&
      profile.role === 'Admin'
    ) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    }
    // NOTE: Only some fields are editable
    const profileId = Profiles.update(
      { _id: id },
      {
        $set: {
          firstName: profile.firstName,
          lastName: profile.lastName,
          address1: profile.address1,
          address2: profile.address2,
          city: profile.city,
          state: profile.state,
          zip: profile.zip,
          phone: profile.phone,
          companyId: profile.companyId,
          planId: profile.planId,
          lastModifiedBy: Meteor.userId()
        }
      }
    );

    const roles = _.without(
      Roles.getRolesForUser(existingProfile.userId, profile.companyId),
      'active'
    );

    Roles.removeUsersFromRoles(
      [existingProfile.userId],
      roles,
      existingProfile.companyId
    );
    Roles.createRole(profile.role, { unlessExists: true });
    Roles.addUsersToRoles(
      [existingProfile.userId],
      [profile.role],
      profile.companyId
    );

    return profileId;
  },
  'profiles.resetUserPasswordAsAdmin' (id) {
    if (Meteor.isServer) {
      const currentUser = Profiles.findOne({ userId: Meteor.userId() });

      if (currentUser.role() !== 'Admin') {
        throw new Meteor.Error('not-authorized', 'You are not authorized.');
      }

      const userProfile = Profiles.findOne({_id: id});
      if (!userProfile) {
        throw new Meteor.Error('profile-not-found', 'Profile not found');
      }

      const { defaultPassword, showPasswordInEmail } = Meteor.settings.private.email.passwordReset
      Accounts.setPassword(userProfile.userId, defaultPassword)

      let emailText = `Hi ${userProfile.lastName}, Your password has been reset.`
      const subject = 'Password Reset';
      const { from } = Meteor.settings.private.email;

      if(showPasswordInEmail) {
        emailText = `${emailText} Log in with this password: ${defaultPassword}`
      } else {
        emailText = `${emailText} Request the new password from admin.`
      }
      
      Meteor.call('sendEmail', userProfile.userEmail, from, subject, emailText);

      return "Successfully reset password";
    }

  },
  'profiles.sendVerificationEmail' (userId) {
    console.log('Sending verification email for userId', userId);

    if (Meteor.isServer) {
      Accounts.sendEnrollmentEmail(userId);
    }
  },
  'profiles.toggleActive' (id) {
    if (Meteor.isServer) {
      check(id, String);

      const profile = Profiles.findOne({ _id: id });
      const currentUserProfile = Profiles.findOne({ userId: Meteor.userId() });
      const currentUserIsAnyAdmin = currentUserProfile.role() === 'Admin';

      if (!profile) {
        throw new Meteor.Error('invalid-id', 'The ID is invalid.');
      } else if (
        Meteor.userId() &&
        (!currentUserIsAnyAdmin || profile.userId === Meteor.userId())
      ) {
        throw new Meteor.Error('not-authorized', 'You are not authorized.');
      }

      if (profile.isActive) {
        Roles.removeUsersFromRoles(profile.userId, 'active');
      } else {
        Roles.addUsersToRoles(profile.userId, 'active');
      }

      return Profiles.update({ _id: id }, {
        $set: {
          isActive: !profile.isActive,
          lastModifiedBy: Meteor.userId()
        }
      });
    }
  },
  'profiles.archive' (profile) {
    if (Meteor.isServer) {
      try {
        Meteor.call("profiles.logout", profile);
        Meteor.call("profiles.toggleActive", profile);

        Profiles.update({ _id: profile._id }, {
          $set: {
            isDeleted: true,
            lastModifiedBy: this.userId
          }
        });
        return 'User is now archived, inactive and logged out from all sessions.';
      } catch (error) {
        throw new Meteor.Error('delete', 'Failed to archive account.');
      }
    }
  },
  'profiles.unarchive' (profile) {
    if (Meteor.isServer) {
      try {
        Meteor.call("profiles.toggleActive", profile);

        Profiles.update({ _id: profile._id }, {
          $set: {
            isDeleted: false,
            lastModifiedBy: this.userId
          }
        });
        return 'User is now unarchived and active';
      } catch (error) {
        throw new Meteor.Error('delete', 'Failed to unarchive account.');
      }
    }
  },
  'profiles.delete' (id) {
    if (Meteor.isServer) {
      const currentUserProfile = Profiles.findOne({ userId: Meteor.userId() });
      const currentUserIsAnyAdmin = currentUserProfile.role() === 'Admin';

      if (
        Meteor.userId() &&
        (!currentUserIsAnyAdmin || id === Meteor.userId())
      ) {
        throw new Meteor.Error('not-authorized', 'You are not authorized.');
      } else {
        const deleteProfile = Profiles.findOne({ _id: id });
        const activity = {
          collectionName: collectionName,
          recordId: id,
          action: 'DELETED',
          value: JSON.stringify(deleteProfile),
          createdBy: this.userId
        };
        Meteor.call('activityLogs.add', activity);
        if (deleteProfile._id !== id) {
          throw new Meteor.Error('invalid-id', 'The ID is invalid.');
        }

        try {
          Meteor.users.remove({ _id: deleteProfile.userId });

          Profiles.update(
            { _id: id },
            {
              $set: {
                isDeleted: true,
                lastModifiedBy: Meteor.userId()
              }
            }
          );

          return 'User successfully deleted.';
        } catch (error) {
          throw new Meteor.Error('delete', 'Failed to delete account.');
        }
      }
    }
  },
  'profiles.addUserKey' (id, profile) {
    profile.userKey = Random.id(32);

    if (Meteor.userId() && profile.userId !== Meteor.userId()) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    }

    return Profiles.update(
      { _id: id },
      {
        $set: {
          userKey: profile.userKey,
          lastModifiedBy: Meteor.userId()
        }
      }
    );
  },
  'profiles.logout' (profile) {
    const currentUserProfile = Profiles.findOne({ userId: Meteor.userId() });
    const currentUserRole = currentUserProfile.role();

    if (currentUserRole !== 'Admin') {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    }

    Meteor.users.update({ _id: profile.userId }, { $set: { 'services.resume.loginTokens': [] } }, { multi: true });
  }
});

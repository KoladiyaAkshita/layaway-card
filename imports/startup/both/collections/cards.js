import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Cards = new Mongo.Collection('Cards');
export const CardsSchema = new SimpleSchema({
  customerId: {
    type: String,
      regEx: SimpleSchema.RegEx.Id
    },
  cardNumber: {
    type: String
    },
  denomination: {
    type: String
    },
  initialValue: {
    type: Number,
      decimal: true
    },
  cardBalance: {
    type: Number,
      optional: true,
      decimal: true
    },
  layawayItemIds: {
    type: [String],
      optional: true,
      regEx: SimpleSchema.RegEx.Id
    },
  status: {
    type: String
    },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
});

Cards.attachSchema(CardsSchema);
Meteor.methods({
  'cards.add'(card) {
    card.createdBy = this.userId;
    CardsSchema.clean(card);

    return Cards.insert(card);
  },
  'cards.edit'(id, card) {
    check(id, String);
    card.lastModifiedBy = this.userId;
    CardsSchema.clean(card);

    const updatedCard = Cards.findOne({ _id: id });

    if (this.userId && updatedCard.createdBy !== this.userId) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedCard._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }
    
    updatedCard.customerId = card.customerId;
    updatedCard.cardNumber = card.cardNumber;
    updatedCard.denomination = card.denomination;
    updatedCard.initialValue = card.initialValue;
    updatedCard.cardBalance = card.cardBalance;
    updatedCard.layawayItemIds = card.layawayItemIds;
    updatedCard.status = card.status;
    updatedCard.lastModifiedBy = card.lastModifiedBy;

    return Cards.update({ _id: id }, { $set: updatedCard });
  },
  'cards.delete'(id) {
      check(id, String);

      const card = Cards.findOne({ _id: id });

      if (this.userId && card.createdBy !== this.userId) {
          throw new Meteor.Error('not-authorized', 'You are not authorized.');
      } else if (card._id !== id) {
          throw new Meteor.Error('invalid-id', 'The ID is invalid.');
      }

      return Cards.remove({ _id: id });
  }
});
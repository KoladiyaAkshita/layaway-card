import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const PaymentPlans = new Mongo.Collection('PaymentPlans');
export const PaymentPlansSchema = new SimpleSchema({
  cardId: {
    type: String,
      optional: true,
      regEx: SimpleSchema.RegEx.Id
    },
  frequency: {
    type: String,
      optional: true
    },
  minimumAmount: {
    type: Number,
      decimal: true,
      min: 10
    },
  automaticWithdrawal: {
    type: Boolean
    },
  customerNumber: {
    type: String,
      optional: true
    },
  paymentType: {
    type: String,
      optional: true
    },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
});

PaymentPlans.attachSchema(PaymentPlansSchema);
Meteor.methods({
  'paymentPlans.add'(paymentPlan) {
    paymentPlan.createdBy = this.userId;
    PaymentPlansSchema.clean(paymentPlan);

    return PaymentPlans.insert(paymentPlan);
  },
  'paymentPlans.edit'(id, paymentPlan) {
    check(id, String);
    paymentPlan.lastModifiedBy = this.userId;
    PaymentPlansSchema.clean(paymentPlan);

    const updatedPaymentPlan = PaymentPlans.findOne({ _id: id });

    if (this.userId && updatedPaymentPlan.createdBy !== this.userId) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedPaymentPlan._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }
    
    updatedPaymentPlan.cardId = paymentPlan.cardId;
    updatedPaymentPlan.frequency = paymentPlan.frequency;
    updatedPaymentPlan.minimumAmount = paymentPlan.minimumAmount;
    updatedPaymentPlan.automaticWithdrawal = paymentPlan.automaticWithdrawal;
    updatedPaymentPlan.customerNumber = paymentPlan.customerNumber;
    updatedPaymentPlan.paymentType = paymentPlan.paymentType;
    updatedPaymentPlan.lastModifiedBy = paymentPlan.lastModifiedBy;

    return PaymentPlans.update({ _id: id }, { $set: updatedPaymentPlan });
  },
  'paymentPlans.delete'(id) {
      check(id, String);

      const paymentPlan = PaymentPlans.findOne({ _id: id });

      if (this.userId && paymentPlan.createdBy !== this.userId) {
          throw new Meteor.Error('not-authorized', 'You are not authorized.');
      } else if (paymentPlan._id !== id) {
          throw new Meteor.Error('invalid-id', 'The ID is invalid.');
      }

      return PaymentPlans.remove({ _id: id });
  }
});
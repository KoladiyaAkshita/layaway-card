import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Payments = new Mongo.Collection('Payments');
export const PaymentsSchema = new SimpleSchema({
  paymentPlanId: {
    type: String,
      regEx: SimpleSchema.RegEx.Id
    },
  amount: {
    type: Number,
      decimal: true
    },
  paymentDate: {
    type: Date
    },
  paymentTime: {
    type: String
    },
  paymentType: {
    type: String
    },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
});

Payments.attachSchema(PaymentsSchema);
Meteor.methods({
  'payments.add'(payment) {
    payment.createdBy = this.userId;
    PaymentsSchema.clean(payment);

    return Payments.insert(payment);
  },
  'payments.edit'(id, payment) {
    check(id, String);
    payment.lastModifiedBy = this.userId;
    PaymentsSchema.clean(payment);

    const updatedPayment = Payments.findOne({ _id: id });

    if (this.userId && updatedPayment.createdBy !== this.userId) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedPayment._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }
    
    updatedPayment.paymentPlanId = payment.paymentPlanId;
    updatedPayment.amount = payment.amount;
    updatedPayment.paymentDate = payment.paymentDate;
    updatedPayment.paymentTime = payment.paymentTime;
    updatedPayment.paymentType = payment.paymentType;
    updatedPayment.lastModifiedBy = payment.lastModifiedBy;

    return Payments.update({ _id: id }, { $set: updatedPayment });
  },
  'payments.delete'(id) {
      check(id, String);

      const payment = Payments.findOne({ _id: id });

      if (this.userId && payment.createdBy !== this.userId) {
          throw new Meteor.Error('not-authorized', 'You are not authorized.');
      } else if (payment._id !== id) {
          throw new Meteor.Error('invalid-id', 'The ID is invalid.');
      }

      return Payments.remove({ _id: id });
  }
});
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ItemsSchema } from './items.js';

export const LayawayItems = new Mongo.Collection('LayawayItems');
export const LayawayItemsSchema = new SimpleSchema({
  cardId: {
    type: String,
      regEx: SimpleSchema.RegEx.Id
    },
  item: {
    type: ItemsSchema,
      optional: true
    },
  itemId: {
    type: String,
      optional: true,
      regEx: SimpleSchema.RegEx.Id
    },
  store: {
    type: String
    },
  purchasePrice: {
    type: Number,
      optional: true,
      decimal: true
    },
  purchaseDate: {
    type: Date,
      optional: true
    },
  purchaseTime: {
    type: String,
      optional: true
    },
  status: {
    type: String
    },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
});

LayawayItems.attachSchema(LayawayItemsSchema);
Meteor.methods({
  'layawayItems.add'(layawayItem) {
    layawayItem.createdBy = this.userId;
    LayawayItemsSchema.clean(layawayItem);

    return LayawayItems.insert(layawayItem);
  },
  'layawayItems.edit'(id, layawayItem) {
    check(id, String);
    layawayItem.lastModifiedBy = this.userId;
    LayawayItemsSchema.clean(layawayItem);

    const updatedLayawayItem = LayawayItems.findOne({ _id: id });

    if (this.userId && updatedLayawayItem.createdBy !== this.userId) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedLayawayItem._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }
    
    updatedLayawayItem.cardId = layawayItem.cardId;
    updatedLayawayItem.item = layawayItem.item;
    updatedLayawayItem.itemId = layawayItem.itemId;
    updatedLayawayItem.store = layawayItem.store;
    updatedLayawayItem.purchasePrice = layawayItem.purchasePrice;
    updatedLayawayItem.purchaseDate = layawayItem.purchaseDate;
    updatedLayawayItem.purchaseTime = layawayItem.purchaseTime;
    updatedLayawayItem.status = layawayItem.status;
    updatedLayawayItem.lastModifiedBy = layawayItem.lastModifiedBy;

    return LayawayItems.update({ _id: id }, { $set: updatedLayawayItem });
  },
  'layawayItems.delete'(id) {
      check(id, String);

      const layawayItem = LayawayItems.findOne({ _id: id });

      if (this.userId && layawayItem.createdBy !== this.userId) {
          throw new Meteor.Error('not-authorized', 'You are not authorized.');
      } else if (layawayItem._id !== id) {
          throw new Meteor.Error('invalid-id', 'The ID is invalid.');
      }

      return LayawayItems.remove({ _id: id });
  }
});
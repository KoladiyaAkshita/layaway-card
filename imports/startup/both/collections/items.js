import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Items = new Mongo.Collection('Items');
export const ItemsSchema = new SimpleSchema({
  companyId: {
    type: String,
      regEx: SimpleSchema.RegEx.Id
    },
  companyItemNumber: {
    type: String,
      optional: true
    },
  layawayCardItemNumber: {
    type: String
    },
  name: {
    type: String
    },
  description: {
    type: String,
      optional: true
    },
  category: {
    type: String
    },
  subCategory: {
    type: String,
      optional: true
    },
  price: {
    type: Number,
      decimal: true
    },
  status: {
    type: String,
      defaultValue: Available
    },

  createdBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  lastModifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  // NOTE: https://meteor.hackpad.com/Meteor-Cookbook-Using-Dates-and-Times-qSQCGFc06gH
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdDate: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  lastModifiedDate: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date();
      }
    },
    optional: true
  }
});

Items.attachSchema(ItemsSchema);
Meteor.methods({
  'items.add'(item) {
    item.createdBy = this.userId;
    ItemsSchema.clean(item);

    return Items.insert(item);
  },
  'items.edit'(id, item) {
    check(id, String);
    item.lastModifiedBy = this.userId;
    ItemsSchema.clean(item);

    const updatedItem = Items.findOne({ _id: id });

    if (this.userId && updatedItem.createdBy !== this.userId) {
      throw new Meteor.Error('not-authorized', 'You are not authorized.');
    } else if (updatedItem._id !== id) {
      throw new Meteor.Error('invalid-id', 'The ID is invalid.');
    }
    
    updatedItem.companyId = item.companyId;
    updatedItem.companyItemNumber = item.companyItemNumber;
    updatedItem.layawayCardItemNumber = item.layawayCardItemNumber;
    updatedItem.name = item.name;
    updatedItem.description = item.description;
    updatedItem.category = item.category;
    updatedItem.subCategory = item.subCategory;
    updatedItem.price = item.price;
    updatedItem.status = item.status;
    updatedItem.lastModifiedBy = item.lastModifiedBy;

    return Items.update({ _id: id }, { $set: updatedItem });
  },
  'items.delete'(id) {
      check(id, String);

      const item = Items.findOne({ _id: id });

      if (this.userId && item.createdBy !== this.userId) {
          throw new Meteor.Error('not-authorized', 'You are not authorized.');
      } else if (item._id !== id) {
          throw new Meteor.Error('invalid-id', 'The ID is invalid.');
      }

      return Items.remove({ _id: id });
  }
});
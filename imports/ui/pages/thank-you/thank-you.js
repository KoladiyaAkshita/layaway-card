import './thank-you.html';
import './thank-you.css';
import { Template } from 'meteor/templating';

/*****************************************************************************/
/* thankYou: Event Handlers */
/*****************************************************************************/
Template.thankYou.events({
});

/*****************************************************************************/
/* thankYou: Helpers */
/*****************************************************************************/
Template.thankYou.helpers({
});

/*****************************************************************************/
/* thankYou: Lifecycle Hooks */
/*****************************************************************************/
Template.thankYou.onCreated(function () {
});

Template.thankYou.onRendered(function () {
});

Template.thankYou.onDestroyed(function () {
});

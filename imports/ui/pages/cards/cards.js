import './cards.html';
import './cards.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { Cards } from '../../../startup/both/collections/cards.js';

/*****************************************************************************/
/* cards: Event Handlers */
/*****************************************************************************/
Template.cards.events({
    'click .delete-card-button'(e) {
        e.preventDefault();

        if (window.confirm('Are you sure?')) {
            const cardId = this._id;

            NProgress.start();
            Meteor.call('cards.delete', cardId, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    // Bert.alert('Card deleted.', 'success', 'fixed-top');
                    swal('Success', 'Card deleted.', 'success');
                }

                NProgress.done();
            });
        }
    },
    'click #prevPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (currentPage - 1 >= 1) {
        template.currentPage.set(--currentPage);
      }
    },
    'click #nextPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (hasMorePages(template)) {
        template.currentPage.set(++currentPage);
      }
    },
    'click .pageNumber'(event, template) {
      event.preventDefault();
  
      const currentPage = parseInt(event.currentTarget.text);
  
      template.currentPage.set(currentPage);
    }   
});

/**
 *
 * @param {*} template
 */
const hasMorePages = function (template) {
    const currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
    const totalCards = Counts.get('cardsCount');
  
    return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalCards;
  }
  
/*****************************************************************************/
/* cards: Helpers */
/*****************************************************************************/
Template.cards.helpers({
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    cards: function () {
        return Cards.find();
    },
    loading: function () {
      return Template.instance().paginatedCardsSubscription && !Template.instance().paginatedCardsSubscription.ready();
    },
    cardCount: function () {
      console.log('cardsCount: ' + Counts.get('cardsCount'));
  
      return Counts.get('cardsCount');
    },
    prevPageClass: function () {
      return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
    },
    nextPageClass: function () {
      return hasMorePages() ? '' : 'disabled';
    },
    pageNumbers: function () {
      let pageNumbers = [];
      const cardsCount = Math.ceil(Counts.get('cardsCount') / Meteor.settings.public.recordsPerPage);
  
      for (var i = 0; i < cardsCount; i++) {
        pageNumbers.push(i + 1);
      }
  
      return pageNumbers;
    },
    recordsPerPage: function () {
      return Meteor.settings.public.recordsPerPage;
    }
});

/*****************************************************************************/
/* cards: Lifecycle Hooks */
/*****************************************************************************/
Template.cards.onCreated(function () {
    var self = this;

    self.currentPage = new ReactiveVar(1);

    self.autorun(function () {
        var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

        self.cardsSubscription = Meteor.subscribe('paginatedCardsForUser', skipCount, (Meteor.user() ? Meteor.userId() : ''), {
            onReady: function () {
            },
            onStop: function (error) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                }
            }
        });
    });
});

Template.cards.onRendered(function () {
});

Template.cards.onDestroyed(function () {
});
import './upsert-card.html';
import './upsert-card.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { NProgress } from 'meteor/mrt:nprogress';
import { Random } from 'meteor/random';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Cards } from '../../../../startup/both/collections/cards.js';
import { Profiles } from '../../../../startup/both/collections/profiles.js';

/*****************************************************************************/
/* upsertCard: Event Handlers */
/*****************************************************************************/
Template.upsertCard.events({
    'click button[type=submit]'(e) {
        e.preventDefault();

        const cardId = FlowRouter.getParam('id');
        let card = cardId ? Cards.findOne() : {};
        
        card.customerId = $("#upsert-customer-id").val();
        card.cardNumber = $("#upsert-card-number").val();
        card.denomination = $("#upsert-denomination").val();
        card.initialValue = $("#upsert-initial-value").val();
        card.cardBalance = $("#upsert-card-balance").val();
        card.status = $("#upsert-status").val();



        NProgress.start();

        if (typeof cardId === 'undefined' || cardId === null || cardId === '') {
            Meteor.call('cards.add', card, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/cards/' + results);
                }

                NProgress.done();
            });
        } else {
            Meteor.call('cards.edit', cardId, card, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/cards/' + cardId);
                }

                NProgress.done();
            });
        }
    }  
});

/*****************************************************************************/
/* upsertCard: Helpers */
/*****************************************************************************/
Template.upsertCard.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    dialogTitle: function () {
        const cardId = FlowRouter.getParam('id');
        let dialogTitle = 'Edit Card';

        if (typeof cardId === 'undefined' || cardId === null || cardId === '') {
            dialogTitle = 'Add Card';
        }

        return dialogTitle;
    },
    card: function () {
        return Cards.findOne();
    },
    loading: function () {
      return Template.instance().cardSubscription && !Template.instance().cardSubscription.ready();
    }
    ,
    profiles: function () {
        return Profiles.find();
    }
});

/*****************************************************************************/
/* upsertCard: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertCard.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var cardId = FlowRouter.getParam('id');

        if (typeof cardId !== 'undefined' && cardId !== null && cardId !== '') {
            self.cardSubscription = Meteor.subscribe('card', cardId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }

        
    self.profilesSubscription = Meteor.subscribe('profiles', {
      onReady: function () {
      },
      onStop: function (error) {
          if (error) {
              // Bert.alert(error.reason, "danger", "fixed-top");
              swal('Error', error.reason, 'error');
          }
      }
  });
    });
});

Template.upsertCard.onRendered(function () {
});

Template.upsertCard.onDestroyed(function () {
});
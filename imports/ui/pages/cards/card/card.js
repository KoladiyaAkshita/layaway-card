import './card.html';
import './card.css';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
import { UserSession } from 'meteor/benjaminrh:user-session';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Cards } from '../../../../startup/both/collections/cards.js';

/*****************************************************************************/
/* card: Event Handlers */
/*****************************************************************************/
Template.card.events({
    'click #add-card-layaway-item-id-button'(e) {
        e.preventDefault();

        const cardId = FlowRouter.getParam('id');
        let card = Cards.findOne({ _id: cardId });
        const layawayItemId = $('#card-layaway-item-id').val();

        if (!card.layawayItemIds) {
            card.layawayItemIds = [];
        }

        card.layawayItemIds.push(layawayItemId);

        NProgress.start();
        Meteor.call('cards.edit', cardId, card, function (error, results) {
            if (error) {
                Bert.alert(error.reason, 'danger', 'fixed-top');
            } else {
                $('#card-layaway-item-id').val('');
            }

            NProgress.done();
        });
    },
    'click #delete-card-layaway-item-id-button'(e) {
        e.preventDefault();

        if (window.confirm('Are you sure?')) {
            const cardId = FlowRouter.getParam('id');
            let card = Cards.findOne({ _id: cardId });
            const layawayItemId = e.currentTarget.parentElement.parentElement.id;

            card.layawayItemIds = $.grep(card.layawayItemIds, function (layawayItemIdObj) {
                return layawayItemIdObj !== layawayItemId;
            });

            Meteor.call('cards.edit', cardId, card, function (error, results) {
                if (error) {
                    Bert.alert(error.reason);
                }
            });
        }
    },

});

/*****************************************************************************/
/* card: Helpers */
/*****************************************************************************/
Template.card.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    card: function () {
        return Cards.findOne();
    },
    loading: function () {
      return Template.instance().cardSubscription && !Template.instance().cardSubscription.ready();
    }
});

/*****************************************************************************/
/* card: Lifecycle Hooks */
/*****************************************************************************/
Template.card.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var cardId = FlowRouter.getParam('id');

        if (typeof cardId !== 'undefined' && cardId !== null && cardId !== '') {
            self.cardSubscription = Meteor.subscribe('card', cardId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }
    });
});

Template.card.onRendered(function () {
});

Template.card.onDestroyed(function () {
});
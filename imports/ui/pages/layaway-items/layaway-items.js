import './layaway-items.html';
import './layaway-items.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { LayawayItems } from '../../../startup/both/collections/layaway-items.js';

/*****************************************************************************/
/* layawayItems: Event Handlers */
/*****************************************************************************/
Template.layawayItems.events({
    'click .delete-layawayItem-button'(e) {
        e.preventDefault();

        if (window.confirm('Are you sure?')) {
            const layawayItemId = this._id;

            NProgress.start();
            Meteor.call('layawayItems.delete', layawayItemId, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    // Bert.alert('LayawayItem deleted.', 'success', 'fixed-top');
                    swal('Success', 'LayawayItem deleted.', 'success');
                }

                NProgress.done();
            });
        }
    },
    'click #prevPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (currentPage - 1 >= 1) {
        template.currentPage.set(--currentPage);
      }
    },
    'click #nextPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (hasMorePages(template)) {
        template.currentPage.set(++currentPage);
      }
    },
    'click .pageNumber'(event, template) {
      event.preventDefault();
  
      const currentPage = parseInt(event.currentTarget.text);
  
      template.currentPage.set(currentPage);
    }   
});

/**
 *
 * @param {*} template
 */
const hasMorePages = function (template) {
    const currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
    const totalLayawayItems = Counts.get('layawayItemsCount');
  
    return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalLayawayItems;
  }
  
/*****************************************************************************/
/* layawayItems: Helpers */
/*****************************************************************************/
Template.layawayItems.helpers({
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    layawayItems: function () {
        return LayawayItems.find();
    },
    loading: function () {
      return Template.instance().paginatedLayawayItemsSubscription && !Template.instance().paginatedLayawayItemsSubscription.ready();
    },
    layawayItemCount: function () {
      console.log('layawayItemsCount: ' + Counts.get('layawayItemsCount'));
  
      return Counts.get('layawayItemsCount');
    },
    prevPageClass: function () {
      return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
    },
    nextPageClass: function () {
      return hasMorePages() ? '' : 'disabled';
    },
    pageNumbers: function () {
      let pageNumbers = [];
      const layawayItemsCount = Math.ceil(Counts.get('layawayItemsCount') / Meteor.settings.public.recordsPerPage);
  
      for (var i = 0; i < layawayItemsCount; i++) {
        pageNumbers.push(i + 1);
      }
  
      return pageNumbers;
    },
    recordsPerPage: function () {
      return Meteor.settings.public.recordsPerPage;
    }
});

/*****************************************************************************/
/* layawayItems: Lifecycle Hooks */
/*****************************************************************************/
Template.layawayItems.onCreated(function () {
    var self = this;

    self.currentPage = new ReactiveVar(1);

    self.autorun(function () {
        var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

        self.layawayItemsSubscription = Meteor.subscribe('paginatedLayawayItemsForUser', skipCount, (Meteor.user() ? Meteor.userId() : ''), {
            onReady: function () {
            },
            onStop: function (error) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                }
            }
        });
    });
});

Template.layawayItems.onRendered(function () {
});

Template.layawayItems.onDestroyed(function () {
});
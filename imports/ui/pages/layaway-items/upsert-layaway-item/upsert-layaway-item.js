import './upsert-layaway-item.html';
import './upsert-layaway-item.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { NProgress } from 'meteor/mrt:nprogress';
import { Random } from 'meteor/random';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { LayawayItems } from '../../../../startup/both/collections/layaway-items.js';
import { Cards } from '../../../../startup/both/collections/cards.js';import { Items } from '../../../../startup/both/collections/items.js';

/*****************************************************************************/
/* upsertLayawayItem: Event Handlers */
/*****************************************************************************/
Template.upsertLayawayItem.events({
    'click button[type=submit]'(e) {
        e.preventDefault();

        const layawayItemId = FlowRouter.getParam('id');
        let layawayItem = layawayItemId ? LayawayItems.findOne() : {};
        
        layawayItem.cardId = $("#upsert-card-id").val();
        layawayItem.itemId = $("#upsert-item-id").val();
        layawayItem.store = $("#upsert-store").val();
        layawayItem.purchasePrice = $("#upsert-purchase-price").val();
        layawayItem.purchaseDate = $("#upsert-purchase-date").val();
        layawayItem.purchaseTime = $("#upsert-purchase-time").val();
        layawayItem.status = $("#upsert-status").val();


        if (!layawayItem.item) {
            layawayItem.item = {};
        layawayItem.item.companyItemNumber = $("#upsert-company-item-number").val();
        layawayItem.item.layawayCardItemNumber = $("#upsert-layaway-card-item-number").val();
        layawayItem.item.name = $("#upsert-name").val();
        layawayItem.item.description = $("#upsert-description").val();
        layawayItem.item.category = $("#upsert-category").val();
        layawayItem.item.subCategory = $("#upsert-sub-category").val();
        layawayItem.item.price = $("#upsert-price").val();
        layawayItem.item.status = $("#upsert-status").val();
        }



        NProgress.start();

        if (typeof layawayItemId === 'undefined' || layawayItemId === null || layawayItemId === '') {
            Meteor.call('layawayItems.add', layawayItem, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/layaway-items/' + results);
                }

                NProgress.done();
            });
        } else {
            Meteor.call('layawayItems.edit', layawayItemId, layawayItem, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/layaway-items/' + layawayItemId);
                }

                NProgress.done();
            });
        }
    }  
});

/*****************************************************************************/
/* upsertLayawayItem: Helpers */
/*****************************************************************************/
Template.upsertLayawayItem.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    dialogTitle: function () {
        const layawayItemId = FlowRouter.getParam('id');
        let dialogTitle = 'Edit Layaway Item';

        if (typeof layawayItemId === 'undefined' || layawayItemId === null || layawayItemId === '') {
            dialogTitle = 'Add Layaway Item';
        }

        return dialogTitle;
    },
    layawayItem: function () {
        return LayawayItems.findOne();
    },
    loading: function () {
      return Template.instance().layawayItemSubscription && !Template.instance().layawayItemSubscription.ready();
    }
    ,
    cards: function () {
        return Cards.find();
    }
    ,
    items: function () {
        return Items.find();
    }
});

/*****************************************************************************/
/* upsertLayawayItem: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertLayawayItem.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var layawayItemId = FlowRouter.getParam('id');

        if (typeof layawayItemId !== 'undefined' && layawayItemId !== null && layawayItemId !== '') {
            self.layawayItemSubscription = Meteor.subscribe('layawayItem', layawayItemId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }

        
    self.cardsSubscription = Meteor.subscribe('cards', {
      onReady: function () {
      },
      onStop: function (error) {
          if (error) {
              // Bert.alert(error.reason, "danger", "fixed-top");
              swal('Error', error.reason, 'error');
          }
      }
  });
    self.itemsSubscription = Meteor.subscribe('items', {
      onReady: function () {
      },
      onStop: function (error) {
          if (error) {
              // Bert.alert(error.reason, "danger", "fixed-top");
              swal('Error', error.reason, 'error');
          }
      }
  });
    });
});

Template.upsertLayawayItem.onRendered(function () {
});

Template.upsertLayawayItem.onDestroyed(function () {
});
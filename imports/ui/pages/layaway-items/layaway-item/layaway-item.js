import './layaway-item.html';
import './layaway-item.css';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
import { UserSession } from 'meteor/benjaminrh:user-session';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { LayawayItems } from '../../../../startup/both/collections/layaway-items.js';

/*****************************************************************************/
/* layawayItem: Event Handlers */
/*****************************************************************************/
Template.layawayItem.events({

});

/*****************************************************************************/
/* layawayItem: Helpers */
/*****************************************************************************/
Template.layawayItem.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    layawayItem: function () {
        return LayawayItems.findOne();
    },
    loading: function () {
      return Template.instance().layawayItemSubscription && !Template.instance().layawayItemSubscription.ready();
    }
});

/*****************************************************************************/
/* layawayItem: Lifecycle Hooks */
/*****************************************************************************/
Template.layawayItem.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var layawayItemId = FlowRouter.getParam('id');

        if (typeof layawayItemId !== 'undefined' && layawayItemId !== null && layawayItemId !== '') {
            self.layawayItemSubscription = Meteor.subscribe('layawayItem', layawayItemId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }
    });
});

Template.layawayItem.onRendered(function () {
});

Template.layawayItem.onDestroyed(function () {
});
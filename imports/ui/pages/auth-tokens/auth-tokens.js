import './auth-tokens.html';
import './auth-tokens.css';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { NProgress } from 'meteor/mrt:nprogress';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { AuthTokens } from '../../../startup/both/collections/auth-tokens.js';
import swal from 'sweetalert';

/*****************************************************************************/
/* authTokens: Event Handlers */
/*****************************************************************************/
Template.authTokens.events({
  'click #delete-authToken-button' (e) {
    e.preventDefault();

    if (window.confirm('Are you sure?')) {
      var authTokenId = this._id;

      NProgress.start();
      Meteor.call('authTokens.delete', authTokenId, function (error, results) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
        } else {
          console.log('AuthToken deleted.');
        }

        NProgress.done();
      });
    }
  },
  'click #prevPage' (event, template) {
    event.preventDefault();

    var currentPage = template.currentPage.get();

    if (currentPage - 1 >= 1) {
      template.currentPage.set(--currentPage);
    }
  },
  'click #nextPage' (event, template) {
    event.preventDefault();

    var currentPage = template.currentPage.get();

    if (hasMorePages(template)) {
      template.currentPage.set(++currentPage);
    }
  },
  'click .pageNumber' (event, template) {
    event.preventDefault();

    var currentPage = parseInt(event.currentTarget.text);

    template.currentPage.set(currentPage);
  },
  'click .delete-auth-button' (e, template) {
    e.preventDefault();

    if (window.confirm('Are you sure?')) {
      const authId = this._id;

      NProgress.start();
      Meteor.call('authTokens.delete', authId, function (error, results) {
        if (error) {
          swal('Error', error.reason, 'error');
        } else {
          swal('Success', 'Partner deleted.', 'success');
        }

        NProgress.done();
      });
    }
  }
});

/**
 *
 * @param {*} template
 */
var hasMorePages = function (template) {
  var currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
  var totalAuthTokens = Counts.get('authTokenssCount');

  return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalAuthTokens;
};

/*****************************************************************************/
/* authTokens: Helpers */
/*****************************************************************************/
Template.authTokens.helpers({
  role: function () {
    var profile = Profiles.findOne();
    var role = profile ? profile.role() : '';

    return role;
  },
  authTokens: function () {
    var profile = Profiles.findOne();
    return AuthTokens.find({ companyId: profile.companyId });
  },
  loading: function () {
    return Template.instance().paginatedAuthTokensSubscription && !Template.instance().paginatedAuthTokensSubscription.ready();
  },
  authTokenCount: function () {
    // console.log("authTokensCount: " + Counts.get('authTokensCount'));

    return Counts.get('authTokensCount');
  },
  prevPageClass: function () {
    return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
  },
  nextPageClass: function () {
    return hasMorePages() ? '' : 'disabled';
  },
  pageNumbers: function () {
    var pageNumbers = [];
    var authTokensCount = Math.ceil(Counts.get('authTokensCount') / Meteor.settings.public.recordsPerPage);

    for (var i = 0; i < authTokensCount; i++) {
      pageNumbers.push(i + 1);
    }

    return pageNumbers;
  },
  recordsPerPage: function () {
    return Meteor.settings.public.recordsPerPage;
  }
});

/*****************************************************************************/
/* authTokens: Lifecycle Hooks */
/*****************************************************************************/
Template.authTokens.onCreated(function () {
  var self = this;

  self.currentPage = new ReactiveVar(1);

  self.autorun(function () {
    var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

    self.paginatedAuthTokensSubscription = Meteor.subscribe('paginatedAuthTokens', skipCount, {
      onReady: function () {
      },
      onStop: function (error) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
        }
      }
    });
  });
});

Template.authTokens.onRendered(function () {
});

Template.authTokens.onDestroyed(function () {
});

import './upsert-auth-token.html';
import './upsert-auth-token.css';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { NProgress } from 'meteor/mrt:nprogress';
import { Random } from 'meteor/random';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { AuthTokens } from '../../../../startup/both/collections/auth-tokens.js';
import CryptoJS from 'crypto-js';

/*****************************************************************************/
/* upsertAuthToken: Event Handlers */
/*****************************************************************************/
Template.upsertAuthToken.events({
  'click button[type=submit]' (e) {
    e.preventDefault();

    var authTokenId = FlowRouter.getParam('id');
    var authToken = authTokenId ? AuthTokens.findOne() : {};
    var privateKey = Template.instance().privateKey.get();
    var profile = Profiles.findOne();

    authToken.applicationName = $('#upsert-application-name').val();
    // authToken.url = $("#upsert-url").val();
    authToken.publicKey = Template.instance().publicKey.get();
    authToken.privateKey = CryptoJS.AES.encrypt(privateKey.toString(), profile.userKey).toString();

    NProgress.start();

    if (typeof authTokenId === 'undefined' || authTokenId === null || authTokenId === '') {
      Meteor.call('authTokens.add', authToken, function (error, results) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        } else {
          FlowRouter.go('/company-admin/auth-tokens/' + results);
        }

        NProgress.done();
      });
    } else {
      Meteor.call('authTokens.edit', authTokenId, authToken, function (error, results) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        } else {
          FlowRouter.go('/company-admin/auth-tokens/' + authTokenId);
        }

        NProgress.done();
      });
    }
  }
});

/*****************************************************************************/
/* upsertAuthToken: Helpers */
/*****************************************************************************/
Template.upsertAuthToken.helpers({
  previousUrl: function () {
    var previousUrl = UserSession.get('previousUrl');

    return previousUrl;
  },
  role: function () {
    var profile = Profiles.findOne();
    var role = profile ? profile.role() : '';

    return role;
  },
  dialogTitle: function () {
    var authTokenId = FlowRouter.getParam('id');
    var dialogTitle = 'Edit Auth Token';

    if (typeof authTokenId === 'undefined' || authTokenId === null || authTokenId === '') {
      dialogTitle = 'Add Auth Token';
    }

    return dialogTitle;
  },
  authToken: function () {
    return AuthTokens.findOne();
  },
  publicKey: function () {
    return Template.instance().publicKey.get();
  },
  privateKey: function () {
    return Template.instance().privateKey.get();
  },
  loading: function () {
    return Template.instance().authTokenSubscription && !Template.instance().authTokenSubscription.ready();
  },
  newAuthToken: function () {
    var authTokenId = FlowRouter.getParam('id');

    return typeof authTokenId === 'undefined' || authTokenId === null || authTokenId === '';
  }
});

/*****************************************************************************/
/* upsertAuthToken: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertAuthToken.onCreated(function () {
  var self = this;

  self.publicKey = new ReactiveVar('');
  self.privateKey = new ReactiveVar('');
  self.autorun(function () {
    var authTokenId = FlowRouter.getParam('id');

    if (typeof authTokenId === 'undefined' || authTokenId === null || authTokenId === '') {
      self.publicKey.set(Random.id(32));
      self.privateKey.set(Random.id(32));
    } else {
      self.authTokenSubscription = Meteor.subscribe('authToken', authTokenId, {
        onReady: function () {
          var authToken = AuthTokens.findOne();
          var decryptedPrivateKey = UserSession.get('decryptedPrivateKey');

          self.publicKey.set(authToken.publicKey);
          self.privateKey.set(decryptedPrivateKey);
        },
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    }
  });
});

Template.upsertAuthToken.onRendered(function () {
});

Template.upsertAuthToken.onDestroyed(function () {
});

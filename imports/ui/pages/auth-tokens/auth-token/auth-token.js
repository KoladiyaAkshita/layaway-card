import './auth-token.html';
import './auth-token.css';
import { Template } from 'meteor/templating';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { AuthTokens } from '../../../../startup/both/collections/auth-tokens.js';

/*****************************************************************************/
/* authToken: Event Handlers */
/*****************************************************************************/
Template.authToken.events({

});

/*****************************************************************************/
/* authToken: Helpers */
/*****************************************************************************/
Template.authToken.helpers({
  previousUrl: function () {
    var previousUrl = UserSession.get('previousUrl');

    return previousUrl;
  },
  role: function () {
    var profile = Profiles.findOne();
    var role = profile ? profile.role() : '';

    return role;
  },
  authToken: function () {
    return AuthTokens.findOne();
  },
  decryptedPrivateKey: function () {
    var decryptedPrivateKey = UserSession.get('decryptedPrivateKey');

    return decryptedPrivateKey;
  },
  loading: function () {
    return Template.instance().authTokenSubscription && !Template.instance().authTokenSubscription.ready();
  }
});

/*****************************************************************************/
/* authToken: Lifecycle Hooks */
/*****************************************************************************/
Template.authToken.onCreated(function () {
  var self = this;

  // self.decryptedPublicKey = new ReactiveVar("");
  self.autorun(function () {
    var authTokenId = FlowRouter.getParam('id');

    if (typeof authTokenId !== 'undefined' && authTokenId !== null && authTokenId !== '') {
      self.authTokenSubscription = Meteor.subscribe('authToken', authTokenId, {
        onReady: function () {
          // var authToken = AuthTokens.findOne();

          // Meteor.call("authToken.decryptedPublicKey", authToken._id, function (error, results) {
          //     if (error) {
          //         // DO SOMETHING
          //     } else {
          //         self.decryptedPublicKey.set(results);
          //     }
          // });
        },
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    }
  });
});

Template.authToken.onRendered(function () {});

Template.authToken.onDestroyed(function () {});

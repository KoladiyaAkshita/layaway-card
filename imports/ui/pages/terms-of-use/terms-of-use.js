import './terms-of-use.html';
import './terms-of-use.css';
import { Template } from 'meteor/templating';

/*****************************************************************************/
/* termsOfUse: Event Handlers */
/*****************************************************************************/
Template.termsOfUse.events({
});

/*****************************************************************************/
/* termsOfUse: Helpers */
/*****************************************************************************/
Template.termsOfUse.helpers({
});

/*****************************************************************************/
/* termsOfUse: Lifecycle Hooks */
/*****************************************************************************/
Template.termsOfUse.onCreated(function () {
});

Template.termsOfUse.onRendered(function () {
});

Template.termsOfUse.onDestroyed(function () {
});

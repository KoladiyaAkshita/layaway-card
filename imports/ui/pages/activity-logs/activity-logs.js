import './activity-logs.html';
import './activity-logs.css';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { ActivityLogs } from '../../../startup/both/collections/activity-logs.js';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';

/*****************************************************************************/
/* activityLogs: Event Handlers */
/*****************************************************************************/
Template.activityLogs.events({
  'click .delete-activityLog-button' (e) {
    e.preventDefault();

    if (window.confirm('Are you sure?')) {
      const activityLogId = this._id;

      NProgress.start();
      Meteor.call('activityLogs.delete', activityLogId, function (error, results) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        } else {
          // Bert.alert('ActivityLog deleted.', "success", "fixed-top");
          swal('Success', 'ActivityLog deleted.', 'success');
        }

        NProgress.done();
      });
    }
  },
  'click #prevPage' (event, template) {
    event.preventDefault();

    let currentPage = template.currentPage.get();

    if (currentPage - 1 >= 1) {
      template.currentPage.set(--currentPage);
    }
  },
  'click #nextPage' (event, template) {
    event.preventDefault();

    let currentPage = template.currentPage.get();

    if (hasMorePages(template)) {
      template.currentPage.set(++currentPage);
    }
  },
  'click .pageNumber' (event, template) {
    event.preventDefault();

    const currentPage = parseInt(event.currentTarget.text);

    template.currentPage.set(currentPage);
  }
});

/**
 *
 * @param {*} template
 */
const hasMorePages = function (template) {
  const currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
  const totalActivityLogs = Counts.get('activityLogsCount');

  return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalActivityLogs;
};

/*****************************************************************************/
/* activityLogs: Helpers */
/*****************************************************************************/
Template.activityLogs.helpers({
  role: function () {
    const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
    const role = profile ? profile.role() : '';

    return role;
  },
  activityLogs: function () {
    return ActivityLogs.find();
  },
  loading: function () {
    return Template.instance().paginatedActivityLogsSubscription && !Template.instance().paginatedActivityLogsSubscription.ready();
  },
  activityLogCount: function () {
    console.log('activityLogsCount: ' + Counts.get('activityLogsCount'));

    return Counts.get('activityLogsCount');
  },
  prevPageClass: function () {
    return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
  },
  nextPageClass: function () {
    return hasMorePages() ? '' : 'disabled';
  },
  pageNumbers: function () {
    const pageNumbers = [];
    const activityLogsCount = Math.ceil(Counts.get('activityLogsCount') / Meteor.settings.public.recordsPerPage);

    for (var i = 0; i < activityLogsCount; i++) {
      pageNumbers.push(i + 1);
    }

    return pageNumbers;
  },
  currentPage: function () {
    return Template.instance().currentPage.get();
  },
  activePageClass: function (index) {
    const currentPage = Template.instance().currentPage.get();
    if (index === currentPage) {
      return 'active';
    }
    return '';
  },
  recordsPerPage: function () {
    return Meteor.settings.public.recordsPerPage;
  }
});

/*****************************************************************************/
/* activityLogs: Lifecycle Hooks */
/*****************************************************************************/
Template.activityLogs.onCreated(function () {
  var self = this;

  self.currentPage = new ReactiveVar(1);

  self.autorun(function () {
    var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

    self.activityLogsSubscription = Meteor.subscribe('paginatedActivityLogsForUser', skipCount, (Meteor.user() ? Meteor.userId() : ''), {
      onReady: function () {
      },
      onStop: function (error) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
        }
      }
    });
  });
});

Template.activityLogs.onRendered(function () {
});

Template.activityLogs.onDestroyed(function () {
});

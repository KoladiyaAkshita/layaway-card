import './upsert-profile.html';
import './upsert-profile.css';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Roles } from 'meteor/alanning:roles';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Companies } from '../../../../startup/both/collections/companies.js';
import { Utils } from '../../../../startup/client/utils';

/*****************************************************************************/
/* upsertProfile: Event Handlers */
/*****************************************************************************/
Template.upsertProfile.events({
  'click button[type=submit]' (e) {
    e.preventDefault();

    const profileId = FlowRouter.getParam('id');
    const currentUserProfile = Profiles.findOne({ userId: (Meteor.user ? Meteor.userId() : '') });
    const currentUserIsAnyAdmin = currentUserProfile.role() === 'Admin' || currentUserProfile.role() === 'CompanyAdmin';

    if ((typeof profileId === 'undefined' || profileId === null || profileId === '') && currentUserIsAnyAdmin) {
      createProfile(currentUserProfile);
    } else {
      editProfile(currentUserProfile, profileId);
    }
  }
});

/**
 *
 * @param {*} currentUserProfile
 */
function createProfile (currentUserProfile) {
  const profile = {
    // NOTE: user email cannot be changed once account is created.
    userEmail: $('#upsert-user-email').val(),
    firstName: $('#upsert-first-name').val(),
    lastName: $('#upsert-last-name').val(),
    role: $('#upsert-role').val(),
    phone: $('#upsert-phone').val(),
    companyId: currentUserProfile.companyId
  };

  NProgress.start();

  if (currentUserProfile.role() === 'CompanyAdmin') {
    Meteor.call('profiles.addAsCompanyAdmin', profile, function (error, results) {
      if (error) {
        // Bert.alert(error.reason, "danger", "fixed-top");
        swal('Error', error.reason, 'error');
        NProgress.done();
      } else {
        // if (typeof callback === 'function') {
        //   callback(results);
        // } else {
        // Bert.alert("An extra user has been added to your plan.", "success", "fixed-top");
        swal('Success', 'User has been successfully added.', 'success');
        NProgress.done();
        FlowRouter.go(`${Utils.determineProfileRoute()}/${results}`);
      }
      // }
    });
  } else if (currentUserProfile.role() === 'Admin') {
    Meteor.call('profiles.addAsAdmin', profile, function (error, results) {
      if (error) {
        // Bert.alert(error.reason, "danger", "fixed-top");
        swal('Error', error.reason, 'error');
        NProgress.done();
      } else {
        swal('Success', 'User has been successfully added.', 'success');
        NProgress.done();
        FlowRouter.go(`${Utils.determineProfileRoute()}/${results}`);
      }
    });
  }
}

/**
 *
 * @param {*} currentUserProfile
 * @param {*} profileId
 */
function editProfile (currentUserProfile, profileId) {
  const currentUserRole = currentUserProfile.role();
  const currentUserIsAnyAdmin = currentUserRole === 'Admin' || currentUserRole === 'CompanyAdmin';

  // NOTE: Only Admins can edit another user's profile
  if (!currentUserIsAnyAdmin && currentUserProfile._id !== profileId) {
    // Bert.alert("You're not allowed to edit someone else's profile.", "danger", "fixed-top");
    swal('Error', "You're not allowed to edit someone else's profile.", 'error');
  } else {
    const profile = {
      firstName: $('#upsert-first-name').val(),
      lastName: $('#upsert-last-name').val(),
      phone: $('#upsert-phone').val()
    };

    if (currentUserIsAnyAdmin) {
      profile.companyId = currentUserProfile.companyId;
      profile.role = $('#upsert-role').val();
    }

    NProgress.start();
    Meteor.call('profiles.edit', profileId, profile, function (error, results) {
      if (error) {
        // Bert.alert(error.reason, "danger", "fixed-top");
        swal('Error', error.reason, 'error');
        NProgress.done();
      } else {
        NProgress.done();
        FlowRouter.go(`${Utils.determineProfileRoute()}/${profileId}`);
      }
    });
  }
}

/*****************************************************************************/
/* upsertProfile: Helpers */
/*****************************************************************************/
Template.upsertProfile.helpers({
  previousUrl: function () {
    const previousUrl = UserSession.get('previousUrl');

    return previousUrl;
  },
  profile: function () {
    const profileId = FlowRouter.getParam('id');
    let profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
    const role = profile ? profile.role() : '';

    // NOTE: Only Admins can edit another user's profile
    if (profile && (role === 'Admin' || role === 'CompanyAdmin') && profile._id !== profileId) {
      profile = Profiles.findOne({ _id: profileId });
    }

    return profile;
  },
  currentUserRole: function () {
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const currentUserRole = profile ? profile.role() : '';

    return currentUserRole;
  },
  dialogTitle: function () {
    const profileId = FlowRouter.getParam('id');
    let dialogTitle = 'Edit User';

    if (typeof profileId === 'undefined' || profileId === null || profileId === '') {
      dialogTitle = 'Add User';
    }

    return dialogTitle;
  },
  addingUserProfile: function () {
    const profileId = FlowRouter.getParam('id');

    return typeof profileId === 'undefined' || profileId === null || profileId === '';
  },
  companies: function () {
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const companies = Roles.userIsInRole(Meteor.userId(), 'Admin', profile.companyId) ? Companies.find() : {};

    return companies;
  }
});

/*****************************************************************************/
/* upsertProfile: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertProfile.onCreated(function () {
  const self = this;

  self.autorun(function () {
    const profileId = FlowRouter.getParam('id');

    if (typeof profileId === 'undefined' || profileId === null || profileId === '') {
      self.profileSubscription = Meteor.subscribe('userProfile', (Meteor.user() ? Meteor.userId() : ''), {
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    } else {
      self.profileSubscription = Meteor.subscribe('profile', profileId, {
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    }

    self.companiesSubscription = Meteor.subscribe('companies', {
      onStop: function (error) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        }
      }
    });
  });
});

Template.upsertProfile.onRendered(function () {
});

Template.upsertProfile.onDestroyed(function () {
});

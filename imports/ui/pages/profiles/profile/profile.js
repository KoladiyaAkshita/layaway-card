import './profile.html';
import './profile.css';
import { Template } from 'meteor/templating';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { $ } from 'meteor/jquery';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Profiles } from '../../../../startup/both/collections/profiles.js';

/*****************************************************************************/
/* profile: Event Handlers */
/*****************************************************************************/
Template.profile.events({
  'click span[id=js-toggle-activity-logs]' () {
    const activityLogsElem = $('#js-activity-logs');

    activityLogsElem.toggle('slow', function () {
      if (activityLogsElem.is(':hidden')) {
        $('#js-toggle-activity-logs').html('[ + ]');
      } else {
        $('#js-toggle-activity-logs').html('[ - ]');
      }
    });
  }
});

/*****************************************************************************/
/* profile: Helpers */
/*****************************************************************************/
Template.profile.helpers({
  previousUrl: function () {
    const previousUrl = UserSession.get('previousUrl');

    return previousUrl;
  },
  profile: function () {
    return getProfile();
  },
  currentUserRole: function () {
    var profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
    var currentUserRole = profile ? profile.role() : '';

    return currentUserRole;
  }
  // activityLogs: function () {
  //     profile = getProfile();

  //     return ActivityLogs.find({ createdBy: profile.userId }, { sort: { createdDate: -1 } });
  // }
});

/**
 *
 */
function getProfile () {
  const profileId = FlowRouter.getParam('id');
  let profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });

  // NOTE: Only Admins & CompanyAdmins can view another user's profile
  if (profile && profileId && (profile.role() === 'Admin' || profile.role() === 'CompanyAdmin') && profile._id !== profileId) {
    profile = Profiles.findOne({ _id: profileId });
  }

  return profile;
}

/*****************************************************************************/
/* profile: Lifecycle Hooks */
/*****************************************************************************/
Template.profile.onCreated(function () {
  var self = this;
  self.currentPage = new ReactiveVar(1);

  self.autorun(function () {
    const profileId = FlowRouter.getParam('id');

    if (typeof profileId === 'undefined' || profileId === null || profileId === '') {
      self.profileSubscription = Meteor.subscribe('userProfile', (Meteor.user() ? Meteor.userId() : ''), {
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    } else {
      self.profileSubscription = Meteor.subscribe('profile', profileId, {
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    }
  });
});

Template.profile.onRendered(function () {
});

Template.profile.onDestroyed(function () {
});

import './profiles.html';
import './profiles.css';
import { Template } from 'meteor/templating';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';

/*****************************************************************************/
/* profiles: Event Handlers */
/*****************************************************************************/
Template.profiles.events({
  'click .send-verification-mail-button' (e, template) {
    e.preventDefault();

    if (confirm('Send Verification Email')) {
      const profileId = this.profile._id;
      const profile = Profiles.findOne({ _id: profileId });

      if (profile) {
        try {
          Meteor.call('profiles.sendVerificationEmail', profile.userId);
          Bert.alert(`Verification e-mail sent to ${profile.userEmail}`, 'success', 'fixed-top');
        } catch (error) {
          // console.log(error);
          Bert.alert('Could not send email', 'danger', 'fixed-top');
        }
      } else {
        Bert.alert('Could not send email', 'danger', 'fixed-top');
      }
    }
  },
  'click .toggle-active-button' (e, template) {
    e.preventDefault();

    const profile = Profiles.findOne({ _id: this._id });

    // Prevent Admin from making themself inactive
    if (this.userId === Meteor.userId()) {
      // Bert.alert('Not a valid operation for your account.', 'danger', 'fixed-top');
      swal('Error', 'Not a valid operation for your account.', 'error');
    } else {
      Meteor.call('profiles.toggleActive', this._id, function (error, results) {
        if (error) {
          swal('Error', error, 'error');
        } else {
          // Logout user
          Meteor.call('profiles.logout', profile, function (error, results) {
            if (error) {
              // Bert.alert('User not logged out from all sessions.', 'danger', 'fixed-top');
              swal('Error', 'User not logged out from all sessions.', 'error');
            }
          });
        }
      });
    }
  },
  'click a[id=archive-profile-button]'(e, template) {
      e.preventDefault();
      var userProfile = this;
      var profile = Meteor.user() ? Profiles.findOne({ userId: Meteor.userId() }) : "";

      if (profile.role() !== "Admin") {
          Bert.alert("You're not allowed to archive user profile.", "danger", "fixed-top");
      } else {
          Meteor.call("profiles.archive", userProfile, function (error, response) {
              if (error) {
                  Bert.alert(error, "danger", "fixed-top");
              } else {
                  Bert.alert(response, "success", "fixed-top");
              }
          })
      }
  },
  'click a[id=unarchive-profile-button]'(e, template) {
      e.preventDefault();
      var userProfile = this;
      var profile = Meteor.user() ? Profiles.findOne({ userId: Meteor.userId() }) : "";

      if (profile.role() !== "Admin") {
          Bert.alert("You're not allowed to unarchive user profile.", "danger", "fixed-top");
      } else {
          Meteor.call("profiles.unarchive", userProfile, function (error, response) {
              if (error) {
                  Bert.alert(error, "danger", "fixed-top");
              } else {
                  Bert.alert(response, "success", "fixed-top");
              }
          })
      }
  },
  'click .delete-profile-button' (e, template) {
    e.preventDefault();

    if (window.confirm('Are you sure?')) {
      const deleteUserProfile = this;
      const profile = Meteor.user() ? Profiles.findOne({ userId: Meteor.userId() }) : '';

      if (profile.role() !== 'Admin' || deleteUserProfile.userId === Meteor.userId()) {
        // Bert.alert('You're not allowed to delete user profile.', 'danger', 'fixed-top');
      } else {
        Meteor.call('profiles.delete', deleteUserProfile._id, function (error, response) {
          if (error) {
            swal('Error', error, 'error');
          } else {
            // Bert.alert(response, 'success', 'fixed-top');
            swal('Success', response, 'success');
          }
        });
      }
    }
  }
});

/*****************************************************************************/
/* profiles: Helpers */
/*****************************************************************************/
Template.profiles.helpers({
  currentUserRole: function () {
    const profile = Profiles.findOne({ userId: Meteor.userId() });
    const currentUserRole = profile ? profile.role() : '';

    return currentUserRole;
  },
  profiles: function () {
    return Profiles.find();
  },
  loading: function () {
    return Template.instance().profilesSubscription && !Template.instance().profilesSubscription.ready();
  }
});

/*****************************************************************************/
/* profiles: Lifecycle Hooks */
/*****************************************************************************/
Template.profiles.onCreated(function () {
  var self = this;

  self.currentPlan = new ReactiveVar('');

  self.autorun(function () {
    self.profilesSubscription = Meteor.subscribe('profiles', {
      onStop: function (error) {
        if (error) {
          // Bert.alert(error.reason, 'danger', 'fixed-top');
          swal('Error', error.reason, 'error');
        }
      }
    });
  });
});

Template.profiles.onRendered(function () {
});

Template.profiles.onDestroyed(function () {
});

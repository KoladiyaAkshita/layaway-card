import './company.html';
import './company.css';
import { Template } from 'meteor/templating';
import { UserSession } from 'meteor/benjaminrh:user-session';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Companies } from '../../../../startup/both/collections/companies.js';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Roles } from 'meteor/alanning:roles';

/*****************************************************************************/
/* company: Event Handlers */
/*****************************************************************************/
Template.company.events({
});

/*****************************************************************************/
/* company: Helpers */
/*****************************************************************************/
Template.company.helpers({
  previousUrl: function () {
    var previousUrl = UserSession.get('previousUrl');

    return previousUrl;
  },
  role: function () {
    var profile = Profiles.findOne();
    var role = profile ? profile.role() : '';

    return role;
  },
  company: function () {
    return Companies.findOne();
  },
  loading: function () {
    return Template.instance().companySubscription && !Template.instance().companySubscription.ready();
  },
  profileId: function () {
    var profileId = FlowRouter.getParam('profileId');
    var profile = Profiles.findOne();

    if (!profileId && profile && Roles.userIsInRole(Meteor.userId(), 'Admin') && profile.companyId) {
      profileId = profile._id;
    }

    return profileId;
  }
});

/*****************************************************************************/
/* company: Lifecycle Hooks */
/*****************************************************************************/
Template.company.onCreated(function () {
  var self = this;

  self.autorun(function () {
    var companyId = FlowRouter.getParam('id');

    if (typeof companyId !== 'undefined' && companyId !== null && companyId !== '') {
      self.companySubscription = Meteor.subscribe('company', companyId, {
        onReady: function () {
        },
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    }
  });
});

Template.company.onRendered(function () {
});

Template.company.onDestroyed(function () {
});

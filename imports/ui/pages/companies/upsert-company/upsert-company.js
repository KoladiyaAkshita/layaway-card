import './upsert-company.html';
import './upsert-company.css';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Companies } from '../../../../startup/both/collections/companies.js';
import { Utils } from '../../../../startup/client/utils';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';

/*****************************************************************************/
/* upsertCompany: Event Handlers */
/*****************************************************************************/
Template.upsertCompany.events({
  'click button[type=submit]' (e) {
    e.preventDefault();

    const companyId = FlowRouter.getParam('id');
    const company = companyId ? Companies.findOne() : {};

    company.name = $('#upsert-name').val();
    company.phone = $('#upsert-phone').val();
    company.website = $('#upsert-website').val();
    company.address1 = $('#upsert-address1').val();
    company.address2 = $('#upsert-address2').val();
    company.city = $('#upsert-city').val();
    company.state = $('#upsert-state').val();
    company.zip = $('#upsert-zip').val();

    NProgress.start();

    if (typeof companyId === 'undefined' || companyId === null || companyId === '') {
      Meteor.call('companies.add', company, function (error, results) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        } else {
          FlowRouter.go(`${Utils.determineRoleRoute()}/companies/${results}`);
        }

        NProgress.done();
      });
    } else {
      Meteor.call('companies.edit', companyId, company, function (error, results) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        } else {
          FlowRouter.go(`${Utils.determineRoleRoute()}/companies/${companyId}`);
        }

        NProgress.done();
      });
    }
  }
});

/*****************************************************************************/
/* upsertCompany: Helpers */
/*****************************************************************************/
Template.upsertCompany.helpers({
  previousUrl: function () {
    const previousUrl = UserSession.get('previousUrl');

    return previousUrl;
  },
  role: function () {
    const profile = Profiles.findOne();
    const role = profile ? profile.role() : '';

    return role;
  },
  dialogTitle: function () {
    const companyId = FlowRouter.getParam('id');
    let dialogTitle = 'Edit Company';

    if (typeof companyId === 'undefined' || companyId === null || companyId === '') {
      dialogTitle = 'Add Company';
    }

    return dialogTitle;
  },
  company: function () {
    const companyId = FlowRouter.getParam('id');

    return Companies.findOne({ _id: companyId });
  },
  loading: function () {
    return Template.instance().companySubscription && !Template.instance().companySubscription.ready();
  },
  states: function () {
    return Meteor.settings.public.states;
  }
});

/*****************************************************************************/
/* upsertCompany: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertCompany.onCreated(function () {
  var self = this;

  self.autorun(function () {
    const companyId = FlowRouter.getParam('id');

    if (typeof companyId !== 'undefined' && companyId !== null && companyId !== '') {
      self.companySubscription = Meteor.subscribe('company', companyId, {
        onReady: function () {
        },
        onStop: function (error) {
          if (error) {
            // Bert.alert(error.reason, "danger", "fixed-top");
            swal('Error', error.reason, 'error');
          }
        }
      });
    }
  });
});

Template.upsertCompany.onRendered(function () {
});

Template.upsertCompany.onDestroyed(function () {
});

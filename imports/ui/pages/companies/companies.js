import './companies.html';
import './companies.css';
import { Template } from 'meteor/templating';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { Companies } from '../../../startup/both/collections/companies.js';

/*****************************************************************************/
/* companies: Event Handlers */
/*****************************************************************************/
Template.companies.events({
  'click #prevPage' (event, template) {
    event.preventDefault();

    var currentPage = template.currentPage.get();

    if (currentPage - 1 >= 1) {
      template.currentPage.set(--currentPage);
    }
  },
  'click #nextPage' (event, template) {
    event.preventDefault();

    var currentPage = template.currentPage.get();

    if (hasMorePages(template)) {
      template.currentPage.set(++currentPage);
    }
  },
  'click .pageNumber' (event, template) {
    event.preventDefault();

    var currentPage = parseInt(event.currentTarget.text);

    template.currentPage.set(currentPage);
  }
});

/**
 *
 * @param {*} template
 */
var hasMorePages = function (template) {
  var currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
  var totalCompanies = Counts.get('companiessCount');

  return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalCompanies;
};

/*****************************************************************************/
/* companies: Helpers */
/*****************************************************************************/
Template.companies.helpers({
  role: function () {
    var profile = Profiles.findOne();
    var role = profile ? profile.role() : '';

    return role;
  },
  companies: function () {
    return Companies.find();
  },
  loading: function () {
    return Template.instance().paginatedCompaniesSubscription && !Template.instance().paginatedCompaniesSubscription.ready();
  },
  companyCount: function () {
    // console.log("companiesCount: " + Counts.get('companiesCount'));

    return Counts.get('companiesCount');
  },
  prevPageClass: function () {
    return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
  },
  nextPageClass: function () {
    return hasMorePages() ? '' : 'disabled';
  },
  pageNumbers: function () {
    var pageNumbers = [];
    var companiesCount = Math.ceil(Counts.get('companiesCount') / Meteor.settings.public.recordsPerPage);

    for (var i = 0; i < companiesCount; i++) {
      pageNumbers.push(i + 1);
    }

    return pageNumbers;
  },
  recordsPerPage: function () {
    return Meteor.settings.public.recordsPerPage;
  }
});

/*****************************************************************************/
/* companies: Lifecycle Hooks */
/*****************************************************************************/
Template.companies.onCreated(function () {
  var self = this;

  self.currentPage = new ReactiveVar(1);

  self.autorun(function () {
    var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

    self.companiesSubscription = Meteor.subscribe('companies', skipCount, {
      onReady: function () {
      },
      onStop: function (error) {
        if (error) {
          // Bert.alert(error.reason, "danger", "fixed-top");
          swal('Error', error.reason, 'error');
        }
      }
    });
  });
});

Template.companies.onRendered(function () {
});

Template.companies.onDestroyed(function () {
});

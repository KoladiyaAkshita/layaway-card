import './upsert-payment-plan.html';
import './upsert-payment-plan.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { NProgress } from 'meteor/mrt:nprogress';
import { Random } from 'meteor/random';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { PaymentPlans } from '../../../../startup/both/collections/payment-plans.js';
import { Cards } from '../../../../startup/both/collections/cards.js';

/*****************************************************************************/
/* upsertPaymentPlan: Event Handlers */
/*****************************************************************************/
Template.upsertPaymentPlan.events({
    'click button[type=submit]'(e) {
        e.preventDefault();

        const paymentPlanId = FlowRouter.getParam('id');
        let paymentPlan = paymentPlanId ? PaymentPlans.findOne() : {};
        
        paymentPlan.cardId = $("#upsert-card-id").val();
        paymentPlan.frequency = $("#upsert-frequency").val();
        paymentPlan.minimumAmount = $("#upsert-minimum-amount").val();
        paymentPlan.automaticWithdrawal = $("#upsert-automatic-withdrawal").is(":checked");
        paymentPlan.customerNumber = $("#upsert-customer-number").val();
        paymentPlan.paymentType = $("#upsert-payment-type").val();



        NProgress.start();

        if (typeof paymentPlanId === 'undefined' || paymentPlanId === null || paymentPlanId === '') {
            Meteor.call('paymentPlans.add', paymentPlan, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/payment-plans/' + results);
                }

                NProgress.done();
            });
        } else {
            Meteor.call('paymentPlans.edit', paymentPlanId, paymentPlan, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/payment-plans/' + paymentPlanId);
                }

                NProgress.done();
            });
        }
    }  
});

/*****************************************************************************/
/* upsertPaymentPlan: Helpers */
/*****************************************************************************/
Template.upsertPaymentPlan.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    dialogTitle: function () {
        const paymentPlanId = FlowRouter.getParam('id');
        let dialogTitle = 'Edit Payment Plan';

        if (typeof paymentPlanId === 'undefined' || paymentPlanId === null || paymentPlanId === '') {
            dialogTitle = 'Add Payment Plan';
        }

        return dialogTitle;
    },
    paymentPlan: function () {
        return PaymentPlans.findOne();
    },
    loading: function () {
      return Template.instance().paymentPlanSubscription && !Template.instance().paymentPlanSubscription.ready();
    }
    ,
    cards: function () {
        return Cards.find();
    }
});

/*****************************************************************************/
/* upsertPaymentPlan: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertPaymentPlan.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var paymentPlanId = FlowRouter.getParam('id');

        if (typeof paymentPlanId !== 'undefined' && paymentPlanId !== null && paymentPlanId !== '') {
            self.paymentPlanSubscription = Meteor.subscribe('paymentPlan', paymentPlanId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }

        
    self.cardsSubscription = Meteor.subscribe('cards', {
      onReady: function () {
      },
      onStop: function (error) {
          if (error) {
              // Bert.alert(error.reason, "danger", "fixed-top");
              swal('Error', error.reason, 'error');
          }
      }
  });
    });
});

Template.upsertPaymentPlan.onRendered(function () {
});

Template.upsertPaymentPlan.onDestroyed(function () {
});
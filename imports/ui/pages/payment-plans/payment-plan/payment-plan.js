import './payment-plan.html';
import './payment-plan.css';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
import { UserSession } from 'meteor/benjaminrh:user-session';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { PaymentPlans } from '../../../../startup/both/collections/payment-plans.js';

/*****************************************************************************/
/* paymentPlan: Event Handlers */
/*****************************************************************************/
Template.paymentPlan.events({

});

/*****************************************************************************/
/* paymentPlan: Helpers */
/*****************************************************************************/
Template.paymentPlan.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    paymentPlan: function () {
        return PaymentPlans.findOne();
    },
    loading: function () {
      return Template.instance().paymentPlanSubscription && !Template.instance().paymentPlanSubscription.ready();
    }
});

/*****************************************************************************/
/* paymentPlan: Lifecycle Hooks */
/*****************************************************************************/
Template.paymentPlan.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var paymentPlanId = FlowRouter.getParam('id');

        if (typeof paymentPlanId !== 'undefined' && paymentPlanId !== null && paymentPlanId !== '') {
            self.paymentPlanSubscription = Meteor.subscribe('paymentPlan', paymentPlanId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }
    });
});

Template.paymentPlan.onRendered(function () {
});

Template.paymentPlan.onDestroyed(function () {
});
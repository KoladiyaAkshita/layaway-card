import './payment-plans.html';
import './payment-plans.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { PaymentPlans } from '../../../startup/both/collections/payment-plans.js';

/*****************************************************************************/
/* paymentPlans: Event Handlers */
/*****************************************************************************/
Template.paymentPlans.events({
    'click .delete-paymentPlan-button'(e) {
        e.preventDefault();

        if (window.confirm('Are you sure?')) {
            const paymentPlanId = this._id;

            NProgress.start();
            Meteor.call('paymentPlans.delete', paymentPlanId, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    // Bert.alert('PaymentPlan deleted.', 'success', 'fixed-top');
                    swal('Success', 'PaymentPlan deleted.', 'success');
                }

                NProgress.done();
            });
        }
    },
    'click #prevPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (currentPage - 1 >= 1) {
        template.currentPage.set(--currentPage);
      }
    },
    'click #nextPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (hasMorePages(template)) {
        template.currentPage.set(++currentPage);
      }
    },
    'click .pageNumber'(event, template) {
      event.preventDefault();
  
      const currentPage = parseInt(event.currentTarget.text);
  
      template.currentPage.set(currentPage);
    }   
});

/**
 *
 * @param {*} template
 */
const hasMorePages = function (template) {
    const currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
    const totalPaymentPlans = Counts.get('paymentPlansCount');
  
    return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalPaymentPlans;
  }
  
/*****************************************************************************/
/* paymentPlans: Helpers */
/*****************************************************************************/
Template.paymentPlans.helpers({
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    paymentPlans: function () {
        return PaymentPlans.find();
    },
    loading: function () {
      return Template.instance().paginatedPaymentPlansSubscription && !Template.instance().paginatedPaymentPlansSubscription.ready();
    },
    paymentPlanCount: function () {
      console.log('paymentPlansCount: ' + Counts.get('paymentPlansCount'));
  
      return Counts.get('paymentPlansCount');
    },
    prevPageClass: function () {
      return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
    },
    nextPageClass: function () {
      return hasMorePages() ? '' : 'disabled';
    },
    pageNumbers: function () {
      let pageNumbers = [];
      const paymentPlansCount = Math.ceil(Counts.get('paymentPlansCount') / Meteor.settings.public.recordsPerPage);
  
      for (var i = 0; i < paymentPlansCount; i++) {
        pageNumbers.push(i + 1);
      }
  
      return pageNumbers;
    },
    recordsPerPage: function () {
      return Meteor.settings.public.recordsPerPage;
    }
});

/*****************************************************************************/
/* paymentPlans: Lifecycle Hooks */
/*****************************************************************************/
Template.paymentPlans.onCreated(function () {
    var self = this;

    self.currentPage = new ReactiveVar(1);

    self.autorun(function () {
        var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

        self.paymentPlansSubscription = Meteor.subscribe('paginatedPaymentPlansForUser', skipCount, (Meteor.user() ? Meteor.userId() : ''), {
            onReady: function () {
            },
            onStop: function (error) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                }
            }
        });
    });
});

Template.paymentPlans.onRendered(function () {
});

Template.paymentPlans.onDestroyed(function () {
});
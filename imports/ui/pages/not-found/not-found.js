import './not-found.html';
import './not-found.css';
import { Template } from 'meteor/templating';

/*****************************************************************************/
/* notFound: Event Handlers */
/*****************************************************************************/
Template.notFound.events({
});

/*****************************************************************************/
/* notFound: Helpers */
/*****************************************************************************/
Template.notFound.helpers({
});

/*****************************************************************************/
/* notFound: Lifecycle Hooks */
/*****************************************************************************/
Template.notFound.onCreated(function () {
});

Template.notFound.onRendered(function () {
});

Template.notFound.onDestroyed(function () {
});

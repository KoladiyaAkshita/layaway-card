import './home.html';
import './home.css';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { $ } from 'meteor/jquery';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { NProgress } from 'meteor/mrt:nprogress';

/*****************************************************************************/
/* home: Event Handlers */
/*****************************************************************************/
Template.home.events({
  'click #sign-up-button' (event) {
    // Prevent default browser form submit
    event.preventDefault();

    var name = $('#name').val().split(' ');
    // var firstName = name.length >= 1 ? name[0] : '';
    // var lastName = name.length >= 2 ? name[1] : '';
    var phone = $('#phone').val();
    var userEmail = $('#email').val();

    Meteor.call('sendEmail', 'keno@themullingsgroup.com', 'gotrackr@themullingsgroup.com', 'GoTrackr Demo Request',
            `${name} - ${phone} - ${userEmail}`,
            function (error, results) {
              if (error) {
                // Bert.alert(error.reason, "danger", "fixed-top");
                swal('Error', error.reason, 'error');
              } else {
                // FlowRouter.go('/thank-you');
                $('#signupform').hide('fast');
                $('#thankyou').show('slow');
              }

              NProgress.done();
            });
  }
});

/*****************************************************************************/
/* home: Helpers */
/*****************************************************************************/
Template.home.helpers({
});

/*****************************************************************************/
/* home: Lifecycle Hooks */
/*****************************************************************************/
Template.home.onCreated(function () {
});

Template.home.onRendered(function () {
});

Template.home.onDestroyed(function () {
});

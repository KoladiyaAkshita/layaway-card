import './upsert-item.html';
import './upsert-item.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { NProgress } from 'meteor/mrt:nprogress';
import { Random } from 'meteor/random';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Items } from '../../../../startup/both/collections/items.js';
import { Companies } from '../../../../startup/both/collections/companies.js';

/*****************************************************************************/
/* upsertItem: Event Handlers */
/*****************************************************************************/
Template.upsertItem.events({
    'click button[type=submit]'(e) {
        e.preventDefault();

        const itemId = FlowRouter.getParam('id');
        let item = itemId ? Items.findOne() : {};
        
        item.companyId = $("#upsert-company-id").val();
        item.companyItemNumber = $("#upsert-company-item-number").val();
        item.layawayCardItemNumber = $("#upsert-layaway-card-item-number").val();
        item.name = $("#upsert-name").val();
        item.description = $("#upsert-description").val();
        item.category = $("#upsert-category").val();
        item.subCategory = $("#upsert-sub-category").val();
        item.price = $("#upsert-price").val();
        item.status = $("#upsert-status").val();



        NProgress.start();

        if (typeof itemId === 'undefined' || itemId === null || itemId === '') {
            Meteor.call('items.add', item, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/items/' + results);
                }

                NProgress.done();
            });
        } else {
            Meteor.call('items.edit', itemId, item, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/items/' + itemId);
                }

                NProgress.done();
            });
        }
    }  
});

/*****************************************************************************/
/* upsertItem: Helpers */
/*****************************************************************************/
Template.upsertItem.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    dialogTitle: function () {
        const itemId = FlowRouter.getParam('id');
        let dialogTitle = 'Edit Item';

        if (typeof itemId === 'undefined' || itemId === null || itemId === '') {
            dialogTitle = 'Add Item';
        }

        return dialogTitle;
    },
    item: function () {
        return Items.findOne();
    },
    loading: function () {
      return Template.instance().itemSubscription && !Template.instance().itemSubscription.ready();
    }
    ,
    companies: function () {
        return Companies.find();
    }
});

/*****************************************************************************/
/* upsertItem: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertItem.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var itemId = FlowRouter.getParam('id');

        if (typeof itemId !== 'undefined' && itemId !== null && itemId !== '') {
            self.itemSubscription = Meteor.subscribe('item', itemId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }

        
    self.companiesSubscription = Meteor.subscribe('companies', {
      onReady: function () {
      },
      onStop: function (error) {
          if (error) {
              // Bert.alert(error.reason, "danger", "fixed-top");
              swal('Error', error.reason, 'error');
          }
      }
  });
    });
});

Template.upsertItem.onRendered(function () {
});

Template.upsertItem.onDestroyed(function () {
});
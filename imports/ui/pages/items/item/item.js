import './item.html';
import './item.css';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
import { UserSession } from 'meteor/benjaminrh:user-session';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Items } from '../../../../startup/both/collections/items.js';

/*****************************************************************************/
/* item: Event Handlers */
/*****************************************************************************/
Template.item.events({

});

/*****************************************************************************/
/* item: Helpers */
/*****************************************************************************/
Template.item.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    item: function () {
        return Items.findOne();
    },
    loading: function () {
      return Template.instance().itemSubscription && !Template.instance().itemSubscription.ready();
    }
});

/*****************************************************************************/
/* item: Lifecycle Hooks */
/*****************************************************************************/
Template.item.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var itemId = FlowRouter.getParam('id');

        if (typeof itemId !== 'undefined' && itemId !== null && itemId !== '') {
            self.itemSubscription = Meteor.subscribe('item', itemId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }
    });
});

Template.item.onRendered(function () {
});

Template.item.onDestroyed(function () {
});
import './items.html';
import './items.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { Items } from '../../../startup/both/collections/items.js';

/*****************************************************************************/
/* items: Event Handlers */
/*****************************************************************************/
Template.items.events({
    'click .delete-item-button'(e) {
        e.preventDefault();

        if (window.confirm('Are you sure?')) {
            const itemId = this._id;

            NProgress.start();
            Meteor.call('items.delete', itemId, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    // Bert.alert('Item deleted.', 'success', 'fixed-top');
                    swal('Success', 'Item deleted.', 'success');
                }

                NProgress.done();
            });
        }
    },
    'click #prevPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (currentPage - 1 >= 1) {
        template.currentPage.set(--currentPage);
      }
    },
    'click #nextPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (hasMorePages(template)) {
        template.currentPage.set(++currentPage);
      }
    },
    'click .pageNumber'(event, template) {
      event.preventDefault();
  
      const currentPage = parseInt(event.currentTarget.text);
  
      template.currentPage.set(currentPage);
    }   
});

/**
 *
 * @param {*} template
 */
const hasMorePages = function (template) {
    const currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
    const totalItems = Counts.get('itemsCount');
  
    return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalItems;
  }
  
/*****************************************************************************/
/* items: Helpers */
/*****************************************************************************/
Template.items.helpers({
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    items: function () {
        return Items.find();
    },
    loading: function () {
      return Template.instance().paginatedItemsSubscription && !Template.instance().paginatedItemsSubscription.ready();
    },
    itemCount: function () {
      console.log('itemsCount: ' + Counts.get('itemsCount'));
  
      return Counts.get('itemsCount');
    },
    prevPageClass: function () {
      return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
    },
    nextPageClass: function () {
      return hasMorePages() ? '' : 'disabled';
    },
    pageNumbers: function () {
      let pageNumbers = [];
      const itemsCount = Math.ceil(Counts.get('itemsCount') / Meteor.settings.public.recordsPerPage);
  
      for (var i = 0; i < itemsCount; i++) {
        pageNumbers.push(i + 1);
      }
  
      return pageNumbers;
    },
    recordsPerPage: function () {
      return Meteor.settings.public.recordsPerPage;
    }
});

/*****************************************************************************/
/* items: Lifecycle Hooks */
/*****************************************************************************/
Template.items.onCreated(function () {
    var self = this;

    self.currentPage = new ReactiveVar(1);

    self.autorun(function () {
        var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

        self.itemsSubscription = Meteor.subscribe('paginatedItemsForUser', skipCount, (Meteor.user() ? Meteor.userId() : ''), {
            onReady: function () {
            },
            onStop: function (error) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                }
            }
        });
    });
});

Template.items.onRendered(function () {
});

Template.items.onDestroyed(function () {
});
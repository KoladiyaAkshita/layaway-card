import './payments.html';
import './payments.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Counts } from 'meteor/tmeasday:publish-counts';
import { NProgress } from 'meteor/mrt:nprogress';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { Payments } from '../../../startup/both/collections/payments.js';

/*****************************************************************************/
/* payments: Event Handlers */
/*****************************************************************************/
Template.payments.events({
    'click .delete-payment-button'(e) {
        e.preventDefault();

        if (window.confirm('Are you sure?')) {
            const paymentId = this._id;

            NProgress.start();
            Meteor.call('payments.delete', paymentId, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    // Bert.alert('Payment deleted.', 'success', 'fixed-top');
                    swal('Success', 'Payment deleted.', 'success');
                }

                NProgress.done();
            });
        }
    },
    'click #prevPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (currentPage - 1 >= 1) {
        template.currentPage.set(--currentPage);
      }
    },
    'click #nextPage'(event, template) {
      event.preventDefault();
  
      let currentPage = template.currentPage.get();
  
      if (hasMorePages(template)) {
        template.currentPage.set(++currentPage);
      }
    },
    'click .pageNumber'(event, template) {
      event.preventDefault();
  
      const currentPage = parseInt(event.currentTarget.text);
  
      template.currentPage.set(currentPage);
    }   
});

/**
 *
 * @param {*} template
 */
const hasMorePages = function (template) {
    const currentPage = template ? template.currentPage.get() : Template.instance().currentPage.get();
    const totalPayments = Counts.get('paymentsCount');
  
    return currentPage * parseInt(Meteor.settings.public.recordsPerPage) < totalPayments;
  }
  
/*****************************************************************************/
/* payments: Helpers */
/*****************************************************************************/
Template.payments.helpers({
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    payments: function () {
        return Payments.find();
    },
    loading: function () {
      return Template.instance().paginatedPaymentsSubscription && !Template.instance().paginatedPaymentsSubscription.ready();
    },
    paymentCount: function () {
      console.log('paymentsCount: ' + Counts.get('paymentsCount'));
  
      return Counts.get('paymentsCount');
    },
    prevPageClass: function () {
      return Template.instance().currentPage.get() <= 1 ? 'disabled' : '';
    },
    nextPageClass: function () {
      return hasMorePages() ? '' : 'disabled';
    },
    pageNumbers: function () {
      let pageNumbers = [];
      const paymentsCount = Math.ceil(Counts.get('paymentsCount') / Meteor.settings.public.recordsPerPage);
  
      for (var i = 0; i < paymentsCount; i++) {
        pageNumbers.push(i + 1);
      }
  
      return pageNumbers;
    },
    recordsPerPage: function () {
      return Meteor.settings.public.recordsPerPage;
    }
});

/*****************************************************************************/
/* payments: Lifecycle Hooks */
/*****************************************************************************/
Template.payments.onCreated(function () {
    var self = this;

    self.currentPage = new ReactiveVar(1);

    self.autorun(function () {
        var skipCount = (self.currentPage.get() - 1) * Meteor.settings.public.recordsPerPage;

        self.paymentsSubscription = Meteor.subscribe('paginatedPaymentsForUser', skipCount, (Meteor.user() ? Meteor.userId() : ''), {
            onReady: function () {
            },
            onStop: function (error) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                }
            }
        });
    });
});

Template.payments.onRendered(function () {
});

Template.payments.onDestroyed(function () {
});
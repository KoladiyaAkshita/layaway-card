import './payment.html';
import './payment.css';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Template } from 'meteor/templating';
import { NProgress } from 'meteor/mrt:nprogress';
import { UserSession } from 'meteor/benjaminrh:user-session';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Payments } from '../../../../startup/both/collections/payments.js';

/*****************************************************************************/
/* payment: Event Handlers */
/*****************************************************************************/
Template.payment.events({

});

/*****************************************************************************/
/* payment: Helpers */
/*****************************************************************************/
Template.payment.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    payment: function () {
        return Payments.findOne();
    },
    loading: function () {
      return Template.instance().paymentSubscription && !Template.instance().paymentSubscription.ready();
    }
});

/*****************************************************************************/
/* payment: Lifecycle Hooks */
/*****************************************************************************/
Template.payment.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var paymentId = FlowRouter.getParam('id');

        if (typeof paymentId !== 'undefined' && paymentId !== null && paymentId !== '') {
            self.paymentSubscription = Meteor.subscribe('payment', paymentId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }
    });
});

Template.payment.onRendered(function () {
});

Template.payment.onDestroyed(function () {
});
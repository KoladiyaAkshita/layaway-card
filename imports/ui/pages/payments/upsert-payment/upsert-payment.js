import './upsert-payment.html';
import './upsert-payment.css';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { $ } from 'meteor/jquery';
import { NProgress } from 'meteor/mrt:nprogress';
import { Random } from 'meteor/random';
// import { Bert } from 'meteor/themeteorchef:bert';
import swal from 'sweetalert';
import { UserSession } from 'meteor/benjaminrh:user-session';
import { Profiles } from '../../../../startup/both/collections/profiles.js';
import { Payments } from '../../../../startup/both/collections/payments.js';
import { PaymentPlans } from '../../../../startup/both/collections/payment-plans.js';

/*****************************************************************************/
/* upsertPayment: Event Handlers */
/*****************************************************************************/
Template.upsertPayment.events({
    'click button[type=submit]'(e) {
        e.preventDefault();

        const paymentId = FlowRouter.getParam('id');
        let payment = paymentId ? Payments.findOne() : {};
        
        payment.paymentPlanId = $("#upsert-payment-plan-id").val();
        payment.amount = $("#upsert-amount").val();
        payment.paymentDate = $("#upsert-payment-date").val();
        payment.paymentTime = $("#upsert-payment-time").val();
        payment.paymentType = $("#upsert-payment-type").val();



        NProgress.start();

        if (typeof paymentId === 'undefined' || paymentId === null || paymentId === '') {
            Meteor.call('payments.add', payment, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/payments/' + results);
                }

                NProgress.done();
            });
        } else {
            Meteor.call('payments.edit', paymentId, payment, function (error, results) {
                if (error) {
                    // Bert.alert(error.reason, 'danger', 'fixed-top');
                    swal('Error', error.reason, 'error');
                } else {
                    FlowRouter.go('/payments/' + paymentId);
                }

                NProgress.done();
            });
        }
    }  
});

/*****************************************************************************/
/* upsertPayment: Helpers */
/*****************************************************************************/
Template.upsertPayment.helpers({
    previousUrl: function () {
        const previousUrl = UserSession.get('previousUrl');

        return previousUrl;
    },
    role: function () {
        const profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
        const role = profile ? profile.role() : '';

        return role;
    },
    dialogTitle: function () {
        const paymentId = FlowRouter.getParam('id');
        let dialogTitle = 'Edit Payment';

        if (typeof paymentId === 'undefined' || paymentId === null || paymentId === '') {
            dialogTitle = 'Add Payment';
        }

        return dialogTitle;
    },
    payment: function () {
        return Payments.findOne();
    },
    loading: function () {
      return Template.instance().paymentSubscription && !Template.instance().paymentSubscription.ready();
    }
    ,
    paymentPlans: function () {
        return PaymentPlans.find();
    }
});

/*****************************************************************************/
/* upsertPayment: Lifecycle Hooks */
/*****************************************************************************/
Template.upsertPayment.onCreated(function () {
    var self = this;

    self.autorun(function () {
        var paymentId = FlowRouter.getParam('id');

        if (typeof paymentId !== 'undefined' && paymentId !== null && paymentId !== '') {
            self.paymentSubscription = Meteor.subscribe('payment', paymentId, {
                onReady: function () {
                },
                onStop: function (error) {
                    if (error) {
                        // Bert.alert(error.reason, 'danger', 'fixed-top');
                        swal('Error', error.reason, 'error');
                    }
                }
            });
        }

        
    self.paymentPlansSubscription = Meteor.subscribe('paymentPlans', {
      onReady: function () {
      },
      onStop: function (error) {
          if (error) {
              // Bert.alert(error.reason, "danger", "fixed-top");
              swal('Error', error.reason, 'error');
          }
      }
  });
    });
});

Template.upsertPayment.onRendered(function () {
});

Template.upsertPayment.onDestroyed(function () {
});
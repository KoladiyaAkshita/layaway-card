import './privacy-policy.html';
import './privacy-policy.css';
import { Template } from 'meteor/templating';

/*****************************************************************************/
/* privacyPolicy: Event Handlers */
/*****************************************************************************/
Template.privacyPolicy.events({
});

/*****************************************************************************/
/* privacyPolicy: Helpers */
/*****************************************************************************/
Template.privacyPolicy.helpers({
});

/*****************************************************************************/
/* privacyPolicy: Lifecycle Hooks */
/*****************************************************************************/
Template.privacyPolicy.onCreated(function () {
});

Template.privacyPolicy.onRendered(function () {
});

Template.privacyPolicy.onDestroyed(function () {
});

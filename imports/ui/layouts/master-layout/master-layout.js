import './master-layout.html';
import './master-layout.css';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import swal from 'sweetalert';
import { Profiles } from '../../../startup/both/collections/profiles.js';
import { Meteor } from 'meteor/meteor';
import { $ } from 'meteor/jquery';

/*****************************************************************************/
/* MasterLayout: Event Handlers */
/*****************************************************************************/
Template.MasterLayout.events({
  'click #logout-button': function (event) {
    // Prevent default browser form submit
    event.preventDefault();

    Meteor.logout(function () {
      FlowRouter.go('/');
    });
  },
  'change #search, click #search-button' (e, template) {
    const search = $('#search').val();

    if (search) {
      // const regexSearch = new RegExp(search.trim(), 'i');
      const collection = null; // Collections.findOne({ name: regexSearch });

      if (collection) {
        FlowRouter.go(`/collections/${collection._id}`);
      } else {
        swal('Sorry', 'No results found.', 'info');
      }
    }
  }
});

/*****************************************************************************/
/* MasterLayout: Helpers */
/*****************************************************************************/
Template.MasterLayout.helpers({
  year: function () {
    return new Date().getFullYear();
  },
  role: function () {
    var profile = Profiles.findOne({ userId: (Meteor.user() ? Meteor.userId() : '') });
    var role = profile ? profile.role() : '';

    return role;
  }
});

Template.MasterLayout.onCreated(function () {
});

Template.MasterLayout.onRendered(function () {
  if (FlowRouter.current().route.name === 'home') {
    var navbar = document.getElementById('navbar');

    if (typeof navbar !== 'undefined' && navbar !== null && navbar.style.display === 'none') {
      navbar.style.display = 'block';
    } else {
      navbar.style.display = 'none';
    }
  }

  $('.menu-list').click(function (e) {
    $(e.currentTarget).removeClass('in');
  });
});

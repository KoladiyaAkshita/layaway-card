./setup-local.sh
./setup-github.sh
./setup-servers.sh

echo ''
echo 'Update SendGrid Settings:'
echo 'Update username and password in settings.json'
echo '<%SendGridUsername%>'
echo '<%SendGridPassword%>'
echo ''
echo 'Application now available online at http://<project-title>-staging.herokuapp.com. Replace <project-title> with your project title.'
echo 'Application will soon be running locally at http://localhost:3030'

read -p "Press any key to continue ..."

meteor --settings config/development/settings.json --inspect
